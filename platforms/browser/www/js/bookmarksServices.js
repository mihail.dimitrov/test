angular.module('bookmarkLocalSevice', ['ionic'])
    .factory('BookmarkStorageSevice', ['$q', '$http', '$rootScope', '$ionicPopup', function($q, $http, $rootScope, $ionicPopup) {
        // 'ngStorage' ->  '$localStorage' -> $localStorage
        // This service change status of user (logged or Not)
        function bookmarkPost(bookmark_post, postType) {
            var isExistingpost = false;
            if (postType === "schedule") {
                if (window.localStorage.ionWordpress_schedule !== undefined) {
                    var user_bookmarks = JSON.parse(window.localStorage.ionWordpress_schedule);
                } else {
                    var user_bookmarks = [];
                }
            } else if (postType === "speakers") {
                if (window.localStorage.ionWordpress_speakers !== undefined) {
                    var user_bookmarks = JSON.parse(window.localStorage.ionWordpress_speakers);
                } else {
                    var user_bookmarks = [];
                }
            } else if (postType === "exhibitors") {
                if (window.localStorage.ionWordpress_exhibitors !== undefined) {
                    var user_bookmarks = JSON.parse(window.localStorage.ionWordpress_exhibitors);
                } else {
                    var user_bookmarks = [];
                }
            } else if (postType === "slots") {
                if (window.localStorage.ionWordpress_slots !== undefined) {
                    var user_bookmarks = JSON.parse(window.localStorage.ionWordpress_slots);
                } else {
                    var user_bookmarks = [];
                }
            }


            //check if this post is already saved
            var existing_post = _.find(user_bookmarks, function(post) { return post.id == bookmark_post.id; });

            _.each(user_bookmarks, function(val, key) {
                if (user_bookmarks[key].id == bookmark_post.id) {
                    console.log("Match!");
                    isExistingpost = true;
                }
            });
            if (!isExistingpost) {
                user_bookmarks.push(bookmark_post);
                // var alertPopup = $ionicPopup.alert({
                //     title: 'Successfully added!',
                //     //template: 'Successfully added!',
                //     cssClass: 'app-alert',
                //     okType: 'button-balanced'
                // });
            } else {
                // var alertPopup = $ionicPopup.alert({
                //     title: 'Already exists!',
                //     //template: 'Already exists!',
                //     cssClass: 'app-alert',
                //     okType: 'button-balanced'
                // });
            }

            if (postType === "schedule") {
                window.localStorage.ionWordpress_schedule = JSON.stringify(user_bookmarks);
            } else if (postType === "speakers") {
                window.localStorage.ionWordpress_speakers = JSON.stringify(user_bookmarks);
            } else if (postType === "exhibitors") {
                window.localStorage.ionWordpress_exhibitors = JSON.stringify(user_bookmarks);
            } else if (postType === "slots") {
                window.localStorage.ionWordpress_slots = JSON.stringify(user_bookmarks);
            }

        };

        function getBookmarks(postType) {
            if (postType === "schedule") {
                return JSON.parse(window.localStorage.ionWordpress_schedule || '[]');
            } else if (postType === "speakers") {
                return JSON.parse(window.localStorage.ionWordpress_speakers || '[]');
            } else if (postType === "exhibitors") {
                return JSON.parse(window.localStorage.ionWordpress_exhibitors || '[]');
            } else if (postType === "slots") {
                return JSON.parse(window.localStorage.ionWordpress_slots || '[]');
            }
        };

        function remove(id, postType) {
            if (postType === "schedule") {

                var user_bookmarks = !_.isUndefined(window.localStorage.ionWordpress_schedule) ? JSON.parse(window.localStorage.ionWordpress_schedule) : [];
                //check if this post is already saved
                var remaining_posts = _.filter(user_bookmarks, function(bookmark) { return bookmark.id != id; });
                window.localStorage.ionWordpress_schedule = JSON.stringify(remaining_posts);

            } else if (postType === "speakers") {

                var user_bookmarks = !_.isUndefined(window.localStorage.ionWordpress_speakers) ? JSON.parse(window.localStorage.ionWordpress_speakers) : [];
                //check if this post is already saved
                var remaining_posts = _.filter(user_bookmarks, function(bookmark) { return bookmark.id != id; });
                window.localStorage.ionWordpress_speakers = JSON.stringify(remaining_posts);

            } else if (postType === "exhibitors") {

                var user_bookmarks = !_.isUndefined(window.localStorage.ionWordpress_exhibitors) ? JSON.parse(window.localStorage.ionWordpress_exhibitors) : [];
                //check if this post is already saved
                var remaining_posts = _.filter(user_bookmarks, function(bookmark) { return bookmark.id != id; });
                window.localStorage.ionWordpress_exhibitors = JSON.stringify(remaining_posts);

            } else if (postType === "slots") {

                var user_bookmarks = !_.isUndefined(window.localStorage.ionWordpress_slots) ? JSON.parse(window.localStorage.ionWordpress_slots) : [];
                //check if this post is already saved
                var remaining_posts = _.filter(user_bookmarks, function(bookmark) { return bookmark.id != id; });
                window.localStorage.ionWordpress_slots = JSON.stringify(remaining_posts);

            }
        };

        return {
            bookmarkPost: bookmarkPost,
            getBookmarks: getBookmarks,
            removeBookmarks: remove
        };
    }])