angular.module('app.controllers', ['ionic'])

.config(function($sceProvider, $ionicConfigProvider) {
    $sceProvider.enabled(false);
    $ionicConfigProvider.views.maxCache(0);
    $ionicConfigProvider.views.forwardCache(true);
})

//
.animation('.agendaListAnimation', function() {
    return {
        enter: function(element, done) {
            element.css('display', 'none');
            $(element).fadeIn(500, function() {
                done();
            });

            // $(element).addClass("animated slideInRight", {
            //     complete: function() {
            //         done();
            //     }
            // });
            // slideInLeft / slideInRight / rollIn / fadeIn / fadeInLeft / fadeInRight / fadeInUp / bounceInLeft / bounceInRight / bounceInUp  animated slideInRight
            //$(element).addClass("animated fadeIn");
        },
        leave: function(element, done) {
            $(element).fadeOut(400, function() {
                done();
            });
            //$(element).addClass("animated fadeOut");
        },
        move: function(element, done) {
            element.css('display', 'none');
            $(element).slideDown(10, function() {
                done();
            });
        }
    }
})

.controller('wTC2017Ctrl', ['$scope', '$stateParams', '$state', '$ionicLoading', 'AuthService', '$rootScope', 'UserInfoService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $state, $ionicLoading, AuthService, $rootScope, UserInfoService) {
        $scope.islogged = false;
        $scope.showPager = false;
        $rootScope.$on('change-user-status', function(event, args) {
            $scope.islogged = args;
        });
        AuthService.userIsLoggedIn().then(function(response) {
            if (response === true) {
                //$state.go("menu.myAccount");
            } else {
                //$state.go('menu.wpLogin');
            }
        });
    }
])

.controller('scheduleCtrl', ['$scope', '$stateParams', '$timeout', '$rootScope', '$q', '$state', '$ionicLoading', '$ionicPopup', '$ionicScrollDelegate', 'PostTypeService', 'ApiUrl', 'BookmarkStorageSevice', 'MainVariableService', 'currentScheduleDayId1', 'activeSection1', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $timeout, $rootScope, $q, $state, $ionicLoading, $ionicPopup, $ionicScrollDelegate, PostTypeService, ApiUrl, BookmarkStorageSevice, MainVariableService, currentScheduleDayId1, activeSection1) {
        console.log('-----------Sessions list---------');
        // For Dividers:
        $scope.dividerFunction = function(key) {
            return key;
        };

        function removeElementsByClass(className) {
            var elements = document.getElementsByClassName("divider-element");
            while (elements.length > 0) {
                elements[0].parentNode.removeChild(elements[0]);
            }
        }

        $scope.isBookmark = true;
        // Should comes from API
        // All days taxonomies.
        $scope.days = [
            { 'id': 71, 'label': 'June 11, 2017' },
            { 'id': 72, 'label': 'June 12, 2017' },
            { 'id': 77, 'label': 'June 13, 2017' },
            { 'id': 78, 'label': 'June 14, 2017' }
            // { 'id': 77, 'label': 'June 15, 2017' },
            // { 'id': 77, 'label': 'June 16, 2017' }
        ];
        // // All room taxonomies.
        // $scope.rooms = [
        //     { 'id': 69, 'label': 'Concert Hall' },
        //     { 'id': 70, 'label': 'Per Gynt Hall' },
        //     { 'id': 73, 'label': 'Stage' },
        //     { 'id': 74, 'label': 'Klokkeklang' },
        //     { 'id': 99, 'label': 'Troldtog' },
        //     { 'id': 79, 'label': 'Poster Session' }
        // ];

        //$scope.days = [];
        $scope.rooms = [];

        $ionicLoading.show({
            template: 'Loading...'
        });
        PostTypeService.getTaxonomy(ApiUrl.scheduleRooms).then(function(result) {
            if (result !== false) {
                // PostTypeService.getTaxonomy(ApiUrl.scheduleDays).then(function(result) {
                //     if (result !== false) {
                //         $ionicLoading.hide();
                //         for (var index = 0; index < result.data.length; index++) {
                //             //console.log(result.data[index].name + ' - ' + result.data[index].id);
                //             var currentDay = {
                //                 'id': result.data[index].id,
                //                 'label': result.data[index].name
                //             };
                //             $scope.days.push(currentDay);
                //         }
                //         console.log($scope.days);
                //         loadSchedulePage();
                //     } else {
                //         console.log('Erorr');
                //         PostTypeService.callBackError();
                //     }
                // });
                //console.log(result.data);
                for (var index = 0; index < result.data.length; index++) {
                    //console.log(result.data[index].name + ' - ' + result.data[index].id);
                    var currentRoom = {
                        'id': result.data[index].id,
                        'label': result.data[index].name
                    };
                    $scope.rooms.push(currentRoom);
                }
                //console.log($scope.rooms);
                loadSchedulePage();
            } else {
                console.log('Error');
                PostTypeService.callBackError();
            }
        });

        function loadSchedulePage() {
            $scope.data = {
                // room: $scope.rooms[0].id,
                room: '',
                //day: $scope.days[0].id,
                day: MainVariableService.getScheduleCurrentDay(),
                searchWord: ''
            };
            // Show/hide days slider:
            $scope.daysSlider = false;
            // Show hide search menu.
            $scope.showSearchMenu = false;
            $scope.showSearch = function() {
                $ionicScrollDelegate.scrollTop();
                if ($scope.showSearchMenu === false) {
                    $scope.showSearchMenu = true;
                    // hide days slider:
                    $scope.daysSlider = true;
                    $scope.data.day = '';
                    removeElementsByClass();
                    $rootScope.$emit('change-divide-key', "");
                    $scope.allSessions = [];
                } else {
                    $scope.showSearchMenu = false;
                    // Show days slider:
                    $scope.daysSlider = false;
                    $scope.data.day = $scope.days[0].id;
                    $scope.data.room = '';
                    $scope.activeSection = 1;
                    removeElementsByClass();
                    $rootScope.$emit('change-divide-key', "");
                    $scope.allSessions = [];
                    $scope.getSessionsForCurrentDay($scope.days[0].id);
                }
            };
            // GEt all Data from service.
            $scope.getSessionsForCurrentDay = function(day) {
                $ionicLoading.show({
                    template: 'Loading...'
                });
                var apiLink = ApiUrl.sessionsByDayUrl[0] + day + '&per_page=30';
                PostTypeService.getPostsByDay(apiLink).then(function(result) {
                    console.log(result);
                    if (result !== false) {
                        $ionicLoading.hide();
                        removeElementsByClass();
                        $rootScope.$emit('change-divide-key', "");
                        $scope.allSessions = result.filter(function(session) {
                            if (session.day[0] === $scope.data.day) {
                                if (session.locations.length !== 0) {
                                    return true;
                                }
                            }
                            return false;
                        });
                        //console.log($scope.allSessions);
                    } else {
                        PostTypeService.callBackError();
                    }
                });
            };
            // Logic about days slider.
            $scope.activeSection = MainVariableService.getScheduleCurrentSection();
            $scope.changeSection = function(sectionNumber, dayId) {
                if (dayId !== $scope.data.day) {
                    $scope.activeSection = sectionNumber;
                    $scope.data.day = dayId;
                    MainVariableService.setScheduleCurrentDay(dayId);
                    MainVariableService.setScheduleCurrentSection(sectionNumber);
                    $scope.getSessionsForCurrentDay(dayId);
                }
            };

            $scope.getSessionsForCurrentDay($scope.data.day);

            // Make this query for search functions.
            PostTypeService.getPosts(ApiUrl.sessionsUrl).then(function(response) {
                var result = response.data;
                $scope.search = function(text, room, day) {
                    removeElementsByClass();
                    $rootScope.$emit('change-divide-key', "");
                    $scope.allSessions = [];
                    var searchText = text.toLowerCase();
                    $timeout(function() { $scope.searchBy(searchText, room, day); }, 10);
                };

                $scope.searchBy = function(text, room, day) {
                    if (text === '' && room !== '' && day !== '') {
                        $scope.allSessions = result.filter(function(session) {
                            if (session.day[0] === day && session.locations[0] === room) {
                                return true;
                            }
                            return false;
                        });
                    }
                    if (text === '' && room === '' && day !== '') {
                        $scope.allSessions = result.filter(function(session) {
                            if (session.day[0] === day) {
                                return true;
                            }
                            return false;
                        });
                    }
                    if (text === '' && room !== '' && day === '') {
                        $scope.allSessions = result.filter(function(session) {
                            if (session.locations[0] === room) {
                                return true;
                            }
                            return false;
                        });
                    }
                    if (text !== '' && room !== '' && day !== '') {
                        $scope.allSessions = result.filter(function(session) {
                            var isSpeaker = false;
                            if (session.acf.slots) {
                                for (var index = 0; index < session.acf.slots.length; index++) {
                                    if (session.acf.slots[index].speakers) {
                                        for (var index1 = 0; index1 < session.acf.slots[index].speakers.length; index1++) {
                                            if (session.acf.slots[index].speakers[index1].post_title.toLowerCase().search(text) > -1) {
                                                isSpeaker = true;
                                            }
                                        }
                                    }
                                }
                            }
                            if (session.day[0] === day && session.locations[0] === room && (session.title.rendered.toLowerCase().search(text) > -1 || isSpeaker)) {
                                return true;
                            }

                            return false;
                        });
                    }
                    if (text !== '' && room === '' && day !== '') {
                        $scope.allSessions = result.filter(function(session) {
                            var isSpeaker = false;
                            if (session.acf.slots) {
                                for (var index = 0; index < session.acf.slots.length; index++) {
                                    if (session.acf.slots[index].speakers) {
                                        for (var index1 = 0; index1 < session.acf.slots[index].speakers.length; index1++) {
                                            if (session.acf.slots[index].speakers[index1].post_title.toLowerCase().search(text) > -1) {
                                                isSpeaker = true;
                                            }
                                        }
                                    }
                                }
                            }
                            if (session.day[0] === day && (session.title.rendered.toLowerCase().search(text) > -1 || isSpeaker)) {
                                return true;
                            }
                            return false;
                        });
                    }
                    if (text !== '' && room !== '' && day === '') {
                        $scope.allSessions = result.filter(function(session) {
                            var isSpeaker = false;
                            if (session.acf.slots) {
                                for (var index = 0; index < session.acf.slots.length; index++) {
                                    if (session.acf.slots[index].speakers) {
                                        for (var index1 = 0; index1 < session.acf.slots[index].speakers.length; index1++) {
                                            if (session.acf.slots[index].speakers[index1].post_title.toLowerCase().search(text) > -1) {
                                                isSpeaker = true;
                                            }
                                        }
                                    }
                                }
                            }
                            if (session.locations[0] === room && (session.title.rendered.toLowerCase().search(text) > -1 || isSpeaker)) {
                                return true;
                            }
                            return false;
                        });
                    }
                    if (text !== '' && room === '' && day === '') {
                        $scope.allSessions = result.filter(function(session) {
                            var isSpeaker = false;
                            if (session.acf.slots) {
                                for (var index = 0; index < session.acf.slots.length; index++) {
                                    if (session.acf.slots[index].speakers) {
                                        for (var index1 = 0; index1 < session.acf.slots[index].speakers.length; index1++) {
                                            if (session.acf.slots[index].speakers[index1].post_title.toLowerCase().search(text) > -1) {
                                                isSpeaker = true;
                                            }
                                        }
                                    }
                                }
                            }
                            if (session.title.rendered.toLowerCase().search(text) > -1 || isSpeaker) {
                                return true;
                            }
                            return false;
                        });
                    }
                };
                // Reset search form.
                $scope.resetSearch = function() {
                    $scope.data.room = '';
                    $scope.data.day = '';
                    $scope.data.searchWord = '';
                    removeElementsByClass();
                    $rootScope.$emit('change-divide-key', "");
                    $scope.allSessions = [];
                };
            });

            $scope.addBookmark = function(sessionObj) {
                //console.log('-----Add bookmark page----');
                var favoriteItems = BookmarkStorageSevice.getBookmarks("schedule");
                var isFavoriite = false;

                for (var index = 0; index < favoriteItems.length; index++) {
                    if (sessionObj.id === favoriteItems[index].id) {
                        isFavoriite = true;
                    }
                }
                if (isFavoriite) {
                    $("#" + sessionObj.id).animateCss('jackInTheBox');
                    $("#" + sessionObj.id).removeClass("ion-android-star");
                    $("#" + sessionObj.id).addClass("ion-android-star-outline");
                    BookmarkStorageSevice.removeBookmarks(sessionObj.id, "schedule");
                } else {
                    $("#" + sessionObj.id).animateCss('jackInTheBox');
                    $("#" + sessionObj.id).removeClass("ion-android-star-outline");
                    $("#" + sessionObj.id).addClass("ion-android-star");
                    BookmarkStorageSevice.bookmarkPost(sessionObj, "schedule");
                }
            };
        }
    }

])

.controller('menuCtrl', ['$scope', '$stateParams', '$state', '$ionicLoading', 'AuthService', '$rootScope', '$ionicHistory', 'UserInfoService', 'MenuData', 'BasketData', 'PostTypeService', 'ApiUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $state, $ionicLoading, AuthService, $rootScope, $ionicHistory, UserInfoService, MenuData, BasketData, PostTypeService, ApiUrl) {
        $rootScope.floorplanList = [];
        // Login/logout
        $scope.isLogged = false;
        $rootScope.$on('change-user-status', function(event, args) {
            $scope.isLogged = args;
        });
        $scope.logOutUser = function() {
            AuthService.logOut();
            $state.go('menu.wpLogin');
        };
        // Woocommerce
        $scope.items = MenuData.items;

        var cartItems = BasketData.getBasket();
        $scope.cartItems = cartItems.length;

        $scope.$on('basket', function(event, args) {
            cartItems = BasketData.getBasket();
            $scope.cartItems = cartItems.length;
        });
        // Add Exhibitors on floorplan
        PostTypeService.getPosts(ApiUrl.exhibitorsUrl).then(function(response) {
            if (response !== false) {
                // $scope.currentExhibitors = response.data;
                var dataPages = response.headers()["x-wp-totalpages"];
                var result = response.data;

                for (var index = 1; index < dataPages; index++) {
                    var currentPageNumber = index += 1;
                    PostTypeService.getAllPostsFromNextPage(ApiUrl.exhibitorsUrl, currentPageNumber).then(function(responseNextPage) {
                        result = result.concat(responseNextPage.data);
                        // console.log('$scope.allExhibitors', result);
                        getFloorplanList(result);
                    });
                }
            } else {
                //PostTypeService.callBackError();
                console.log('Something went wrong! - Please check your internet connection!');
            }
        });

        var getFloorplanList = function(result) {
            for (var ii = 0; ii < result.length; ii++) {
                var exhibitor = {};
                if (result[ii].acf) {
                    var boothlist = result[ii].acf.booth.split(", ");
                    for (var jj = 0; jj < boothlist.length; jj++) {
                        exhibitor = { booth_id: boothlist[jj], id: result[ii].id, title: result[ii].title.rendered };
                        $rootScope.floorplanList.push(exhibitor);
                    }

                }
            }
            // console.log('This are all Exhibitors for floorplan');
            // console.log($rootScope.floorplanList);
        }

    }
])

.controller('sponsorsCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', '$ionicLoading', '$ionicScrollDelegate', 'PostTypeService', 'ApiUrl', 'BookmarkStorageSevice', 'MainVariableService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $rootScope, $stateParams, $timeout, $ionicLoading, $ionicScrollDelegate, PostTypeService, ApiUrl, BookmarkStorageSevice, MainVariableService) {
        console.log('-----------Sponsors list---------');
        $scope.dividerFunction = function(key) {
            return key;
        };
        // Remove auto dividers when we select another category.
        function removeElementsByClass(className) {
            $(".sponsors-list .divider-element-alph").remove();
        }
        // Show/hide days slider:
        $scope.daysLevelButtons = true;
        // Show hide search menu.
        $scope.showSearchMenu = false;
        $scope.showSearch = function() {
            $ionicScrollDelegate.scrollTop();
            if ($scope.showSearchMenu === false) {
                $scope.showSearchMenu = true;
                // hide levels buttons:
                $scope.daysLevelButtons = false;
                $scope.data.sponsorCategory = '';
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allSponsors = [];
            } else {
                $scope.showSearchMenu = false;
                // Show levels buttons:
                $scope.daysLevelButtons = true;
                $scope.data.sponsorCategory = $scope.levels[0].id;
                $scope.activeSection = 1;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alhp', "");
                $scope.allSponsors = [];
                $scope.getSponsorsForCurrentLevel($scope.levels[0].id);
            }
        };
        // Sponsors levels.
        $scope.levels = [
            { 'id': 22, 'label': 'Platinum' },
            { 'id': 21, 'label': 'Gold' },
            { 'id': 20, 'label': 'Silver' },
            { 'id': 26, 'label': 'Bronze' }
        ];
        $scope.data = {
            searchWord: '',
            //sponsorCategory: $scope.levels[0].id,
            sponsorCategory: MainVariableService.getSponsorsCurrentLevel(),
        };

        // GEt all Data from service.
        $scope.getSponsorsForCurrentLevel = function(level) {
            $ionicLoading.show({
                template: 'Loading...'
            });
            var apiLink = ApiUrl.exhibitorByLevelUrl[0] + level + '&per_page=40';
            PostTypeService.getPostsByDay(apiLink).then(function(result) {
                if (result !== false) {
                    //console.log(result);
                    $ionicLoading.hide();
                    removeElementsByClass();
                    $rootScope.$emit('change-divide-key', "");
                    $timeout(function() { $scope.allSponsors = result; }, 10);
                } else {
                    PostTypeService.callBackError();
                    //console.log('Something went wrong!');
                }
            });
        };
        // Logic about level buttons.
        //$scope.activeSection = 1;
        $scope.activeSection = MainVariableService.getSponsorCurrentSection();
        $scope.changeSection = function(sectionNumber, category) {
            if ($scope.activeSection !== sectionNumber) {
                $scope.activeSection = sectionNumber;
                $scope.data.sponsorCategory = category;
                MainVariableService.setSponsorsCurrentLevel(category);
                MainVariableService.setSponsorCurrentSection(sectionNumber);
                $scope.getSponsorsForCurrentLevel(category);
            }
        };
        $scope.getSponsorsForCurrentLevel($scope.data.sponsorCategory);

        // Get all sponsors.
        // platinum
        var apiLink = ApiUrl.exhibitorByLevelUrl[0] + ApiUrl.levelPlatinum + '&per_page=40';
        PostTypeService.getPostsByDay(apiLink).then(function(resultPlatinum) {
            var result = resultPlatinum;
            // Gold
            var apiLink = ApiUrl.exhibitorByLevelUrl[0] + ApiUrl.levelGold + '&per_page=40';
            PostTypeService.getPostsByDay(apiLink).then(function(resultGold) {
                result = result.concat(resultGold)
                    // Silver
                var apiLink = ApiUrl.exhibitorByLevelUrl[0] + ApiUrl.levelSilver + '&per_page=40';
                PostTypeService.getPostsByDay(apiLink).then(function(resultSilver) {
                    result = result.concat(resultSilver)
                        // Bronze
                    var apiLink = ApiUrl.exhibitorByLevelUrl[0] + ApiUrl.levelBronze + '&per_page=40';
                    PostTypeService.getPostsByDay(apiLink).then(function(resultBronze) {
                        result = result.concat(resultBronze)
                            //console.log(result);
                    });
                });
            });

            $scope.search = function(text, level) {
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allSponsors = [];
                var searchText = text.toLowerCase();
                $timeout(function() { $scope.searchByTextField(searchText, level); }, 10);
            };

            $scope.searchByTextField = function(text, level) {
                if (text === '' && level !== '') {
                    $scope.allSponsors = result.filter(function(sponsor) {
                        if (sponsor.categories[0] === level) {
                            return true;
                        }
                        return false;
                    });
                }
                if (text !== '' && level === '') {
                    $scope.allSponsors = result.filter(function(sponsor) {
                        if (sponsor.title.rendered.toLowerCase().search(text) > -1) {
                            return true;
                        }
                        return false;
                    });
                }
                if (text !== '' && level !== '') {
                    $scope.allSponsors = result.filter(function(sponsor) {
                        if (sponsor.categories[0] === level && sponsor.title.rendered.toLowerCase().search(text) > -1) {
                            return true;
                        }
                        return false;
                    });
                }
            };

            // Reset search form.
            $scope.resetSearch = function() {
                $scope.data.sponsorCategory = '';
                $scope.data.searchWord = '';
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allSponsors = [];
            };

            // must pass "post" from ng-repeat in the list page
            $scope.addBookmark = function(exhibitorsObj) {
                console.log('-----Add bookmark page----');
                var favoriteItems = BookmarkStorageSevice.getBookmarks("exhibitors");
                console.log(favoriteItems)
                var isFavoriite = false;

                for (var index = 0; index < favoriteItems.length; index++) {
                    if (exhibitorsObj.id === favoriteItems[index].id) {
                        isFavoriite = true;
                    }
                }
                if (isFavoriite) {
                    $("#" + exhibitorsObj.id).animateCss('jackInTheBox');
                    $("#" + exhibitorsObj.id).removeClass("ion-android-star");
                    $("#" + exhibitorsObj.id).addClass("ion-android-star-outline");
                    BookmarkStorageSevice.removeBookmarks(exhibitorsObj.id, "exhibitors");
                } else {
                    $("#" + exhibitorsObj.id).animateCss('jackInTheBox');
                    $("#" + exhibitorsObj.id).removeClass("ion-android-star-outline");
                    $("#" + exhibitorsObj.id).addClass("ion-android-star");
                    BookmarkStorageSevice.bookmarkPost(exhibitorsObj, "exhibitors");
                }
            };

            // $scope.addBookmark = function(sponsorsObj) {
            //     console.log('-----Add bookmark page----');
            //     BookmarkStorageSevice.bookmarkPost(sponsorsObj, "exhibitors");
            // };

        });
    }
])

.controller('exhibitorsCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', '$ionicLoading', '$ionicScrollDelegate', 'PostTypeService', 'ApiUrl', 'BookmarkStorageSevice', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $rootScope, $stateParams, $timeout, $ionicLoading, $ionicScrollDelegate, PostTypeService, ApiUrl, BookmarkStorageSevice) {
        console.log('-----------Exhibitors list---------');
        $scope.dividerFunction = function(key) {
            return key;
        };
        PostTypeService.setPageCounter();
        // Remove auto dividers when we select another category.
        function removeElementsByClass(className) {
            $(".exhibitors-list .divider-element-alph").remove();
        }
        // Show hide load more button.
        $scope.hideLoadMoreButton = false;
        // Show hide search menu.
        $scope.showSearchMenu = false;
        $scope.showSearch = function() {
            $ionicScrollDelegate.scrollTop();
            if ($scope.showSearchMenu === false) {
                $scope.showSearchMenu = true;
                $scope.hideLoadMoreButton = false;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allExhibitors = [];
            } else {
                $scope.showSearchMenu = false;
                $scope.hideLoadMoreButton = true;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alhp', "");
                $scope.allExhibitors = $scope.currentExhibitors;
            }
        };
        $scope.data = {
            searchWord: ''
        };

        $scope.showSpiner = false;
        // Get next 10 exhibitors.
        $scope.loadMoreExhibitors = function() {
            $scope.showSpiner = true;
            PostTypeService.getNext10Posts(ApiUrl.exhibitorUrl).then(function(resultNext10) {
                if (resultNext10 !== false) {
                    $scope.showSpiner = false;
                    var currentListExhibitors = $scope.allExhibitors;
                    $scope.allExhibitors = [];
                    removeElementsByClass();
                    $rootScope.$emit('change-divide-key-alph', "");
                    $timeout(function() {
                        $scope.allExhibitors = currentListExhibitors.concat(resultNext10);
                        $ionicScrollDelegate.scrollBottom(true);
                        console.log($scope.allExhibitors.length);
                    }, 10);
                } else {
                    PostTypeService.callBackError();
                    //console.log('Something went wrong!');
                }
            });
        };
        $ionicLoading.show({
            template: 'Loading...'
        });
        // Get first 10 exhibitors.
        PostTypeService.getFirst10Posts(ApiUrl.exhibitorUrl).then(function(resultFirts10) {
            if (resultFirts10 !== false) {
                $ionicLoading.hide();
                // Assign all exhibitors from db.
                $scope.allExhibitors = resultFirts10;
                $scope.currentExhibitors = resultFirts10;
                $scope.hideLoadMoreButton = true;
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
        // Get all Exhibitors from DB. I will use this for search functionality.
        PostTypeService.getPosts(ApiUrl.exhibitorsUrl).then(function(response) {
            // $scope.currentExhibitors = response.data;
            var dataPages = response.headers()["x-wp-totalpages"];
            var result = response.data;


            for (var index = 1; index < dataPages; index++) {
                var currentPageNumber = index += 1;
                PostTypeService.getAllPostsFromNextPage(ApiUrl.exhibitorsUrl, currentPageNumber).then(function(responseNextPage) {
                    result = result.concat(responseNextPage.data);

                });
            }
            $scope.search = function(text) {
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allExhibitors = [];
                var searchText = text.toLowerCase();
                $timeout(function() { $scope.searchByTextField(searchText) }, 10);
            };
            $scope.searchByTextField = function(text) {
                if (text !== '') {
                    $scope.allExhibitors = result.filter(function(exhibitor) {
                        if (exhibitor.title.rendered.toLowerCase().search(text) > -1) {
                            return true;
                        }
                        return false;
                    });
                }
            };
            // Reset search form.
            $scope.resetSearch = function() {
                $scope.data.searchWord = '';
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allExhibitors = [];
            };

            // must pass "post" from ng-repeat in the list page
            $scope.addBookmark = function(exhibitorsObj) {
                console.log('-----Add bookmark page----');
                var favoriteItems = BookmarkStorageSevice.getBookmarks("exhibitors");
                console.log(favoriteItems)
                var isFavoriite = false;

                for (var index = 0; index < favoriteItems.length; index++) {
                    if (exhibitorsObj.id === favoriteItems[index].id) {
                        isFavoriite = true;
                    }
                }
                if (isFavoriite) {
                    $("#" + exhibitorsObj.id).animateCss('jackInTheBox');
                    $("#" + exhibitorsObj.id).removeClass("ion-android-star");
                    $("#" + exhibitorsObj.id).addClass("ion-android-star-outline");
                    BookmarkStorageSevice.removeBookmarks(exhibitorsObj.id, "exhibitors");
                } else {
                    $("#" + exhibitorsObj.id).animateCss('jackInTheBox');
                    $("#" + exhibitorsObj.id).removeClass("ion-android-star-outline");
                    $("#" + exhibitorsObj.id).addClass("ion-android-star");
                    BookmarkStorageSevice.bookmarkPost(exhibitorsObj, "exhibitors");
                }
            };

            // $scope.addBookmark = function(exhibitorObj) {
            //     console.log('-----Add bookmark page----');
            //     BookmarkStorageSevice.bookmarkPost(exhibitorObj, "exhibitors");
            // };

        });
    }
])

.controller('committeeCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams) {


    }
])

.controller('contactCtrl', ['$scope', '$stateParams', '$ionicLoading', '$cordovaInAppBrowser', 'ApiUrl', 'pagesContentService', 'PostTypeService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, $cordovaInAppBrowser, ApiUrl, pagesContentService, PostTypeService) {
        //var url = ApiUrl.sitePagesLink + ApiUrl.contactPage;
        $ionicLoading.show({
            template: 'Loading...'
        });
        pagesContentService.getContent(ApiUrl.contactPage).then(function(result) {
            if (result !== false) {
                $ionicLoading.hide();
                $scope.content = result.content.rendered;
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }

        });
    }
])

.controller('safetyCtrl', ['$scope', '$stateParams', '$ionicLoading', 'pagesContentService', 'ApiUrl', 'PostTypeService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, pagesContentService, ApiUrl, PostTypeService) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        pagesContentService.getContent(ApiUrl.safetyPage).then(function(result) {
            if (result !== false) {
                $ionicLoading.hide();
                $scope.content = result.content.rendered;
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('floorplanCtrl', ['$scope', '$stateParams', '$ionicLoading', 'pagesContentService', 'ApiUrl', 'PostTypeService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, pagesContentService, ApiUrl, PostTypeService) {
        $scope.showLinkToExhibitorsMap = false;
        $ionicLoading.show({
            template: 'Loading...'
        });
        pagesContentService.getContent(ApiUrl.floorplanImages).then(function(result) {
            if (result !== false) {
                $scope.showLinkToExhibitorsMap = true;
                $ionicLoading.hide();
                $scope.content = result.content.rendered;
            } else {
                PostTypeService.callBackError();
                console.log('Something went wrong!');
            }
        });

    }
])

.controller('floorplan1Ctrl', ['$scope', '$stateParams', '$ionicSlideBoxDelegate', '$ionicPopup', '$state', '$stateParams', '$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $stateParams, $ionicSlideBoxDelegate, $ionicPopup, $state, $stateParams, $rootScope) {

            $scope.showConfirm = function(index) {
                // filter 
                function checkid(item) {
                    return item.booth_id == index;
                }

                var selectedItem = $rootScope.floorplanList.filter(checkid);

                // var confirmPopup = $ionicPopup.confirm({
                //     title: 'Booth ' + index,
                //     template: 'Are you sure you want to go to Detail page?',
                //     okType: 'button-balanced',
                //     cancelType: 'button-calm'
                // });
                // confirmPopup.then(function(res) {
                //     if (res) {
                //         if (selectedItem[0]) {
                //             $state.go('menu.exhibitor', { id: selectedItem[0].id });
                //         }
                //     } else {

                //     }
                // });

                $state.go('menu.exhibitor', { id: selectedItem[0].id });
            };
            // $scope.position1Flag = true;
            $scope.position1Flag = false;
            $scope.position2Flag = false;
            if ($stateParams.id) {
                $scope.position1Flag = true;
            }

            var insteadid = '';

            angular.element(document).ready(function() {
                if ($stateParams.id) {

                    var firstPosList = $('#poly-' + $stateParams.id).attr('points');

                    var secondPosArrary = firstPosList.split(" ");
                    var x = 0,
                        y = 0,
                        count = 0;
                    for (var ii = 0; ii < secondPosArrary.length; ii++) {
                        var thirdposItem = secondPosArrary[ii].split(',');
                        x += parseInt(thirdposItem[0]);
                        y += parseInt(thirdposItem[1]);
                        count++;
                    }
                    var unitpos1 = $('#floorplan-1-img').height() * 1.8 / 3506;
                    var unitpos2 = $('#floorplan-1-img').height() / 1958;
                    var leftPos = unitpos1 * x / count - 10;
                    var topPos = unitpos2 * y / count - 28;

                    $("#floorplan-position1").css({ left: leftPos + 'px', top: topPos + "px" });
                }
                $(".imp-shape-poly").click(function() {
                    var nowid = $(this).attr('id');
                    if (insteadid != '') {
                        $("#" + insteadid).css({ fill: 'rgba(0, 255, 0, 0.4)' });
                    }
                    $("#" + nowid).css({ fill: 'rgba(255, 255, 255, 0.4)' });
                    insteadid = nowid;
                    // getting booth of selected floor plan
                    var booth = nowid.substring(5);
                    $scope.showConfirm(booth);
                });
            });


        }
    ])
    .controller('floorplan2Ctrl', ['$scope', '$stateParams', '$ionicSlideBoxDelegate', '$ionicPopup', '$state', '$stateParams', '$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $stateParams, $ionicSlideBoxDelegate, $ionicPopup, $state, $stateParams, $rootScope) {
            $scope.showConfirm = function(index) {
                function checkid(item) {
                    return item.booth_id == index;
                }
                var selectedItem = $rootScope.floorplanList.filter(checkid);
                // var confirmPopup = $ionicPopup.confirm({
                //     title: 'Booth ' + index,
                //     template: 'Are you sure you want to go to Detail page?',
                //     okType: 'button-balanced',
                //     cancelType: 'button-calm'
                // });
                // confirmPopup.then(function(res) {
                //     if (res) {
                //         if (selectedItem[0]) {
                //             $state.go('menu.exhibitor', { id: selectedItem[0].id });
                //         }
                //     } else {

                //     }
                // });

                $state.go('menu.exhibitor', { id: selectedItem[0].id });
            };
            // $scope.position1Flag = true;
            $scope.position1Flag = false;

            if ($stateParams.id) {
                $scope.position1Flag = true;
            }
            var insteadid = '';
            angular.element(document).ready(function() {
                if ($stateParams.id) {
                    var firstPosList = $('#poly-' + $stateParams.id).attr('points');
                    console.log("firstpost", firstPosList);

                    var secondPosArrary = firstPosList.split(" ");
                    var x = 0,
                        y = 0,
                        count = 0;
                    for (var ii = 0; ii < secondPosArrary.length; ii++) {
                        var thirdposItem = secondPosArrary[ii].split(',');
                        x += parseInt(thirdposItem[0]);
                        y += parseInt(thirdposItem[1]);
                        count++;
                    }
                    var unitpos1 = $('#floorplan-2-img').height() * 1.8 / 3506;
                    var unitpos2 = $('#floorplan-2-img').height() / 1958;
                    var leftPos = unitpos1 * x / count - 10;
                    var topPos = unitpos2 * y / count - 28;

                    $("#floorplan-position1").css({ left: leftPos + 'px', top: topPos + "px" });
                }
                $(".imp-shape-poly").click(function() {
                    var nowid = $(this).attr('id');
                    if (insteadid != '') {
                        $("#" + insteadid).css({ fill: 'rgba(0, 255, 0, 0.4)' });
                    }
                    $("#" + nowid).css({ fill: 'rgba(255, 255, 255, 0.4)' });
                    insteadid = nowid;
                    // getting booth of selected floor plan
                    var booth = nowid.substring(5);
                    $scope.showConfirm(booth);
                });
            });
        }
    ])
    .controller('floorplan3Ctrl', ['$scope', '$stateParams', '$ionicSlideBoxDelegate', '$ionicPopup', '$state', '$stateParams', '$rootScope', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $stateParams, $ionicSlideBoxDelegate, $ionicPopup, $state, $stateParams, $rootScope) {
            $scope.showConfirm = function(index) {
                function checkid(item) {
                    return item.booth_id == index;
                }
                // 
                var selectedItem = $rootScope.floorplanList.filter(checkid);

                // var confirmPopup = $ionicPopup.confirm({
                //     title: 'Booth ' + index,
                //     template: 'Are you sure you want to go to Detail page?',
                //     okType: 'button-balanced',
                //     cancelType: 'button-calm'
                // });
                // confirmPopup.then(function(res) {
                //     if (res) {
                //         if (selectedItem[0]) {
                //             $state.go('menu.exhibitor', { id: selectedItem[0].id });
                //         }
                //     } else {

                //     }
                // });

                $state.go('menu.exhibitor', { id: selectedItem[0].id });
            };
            // $scope.position1Flag = true;
            $scope.position1Flag = false;
            $scope.position2Flag = false;

            if ($stateParams.id) {
                $scope.position1Flag = true;
                $scope.position2Flag = false;
            }
            var insteadid = '';
            angular.element(document).ready(function() {
                if ($stateParams.id) {
                    var firstPosList = $('#poly-' + $stateParams.id).attr('points');

                    var secondPosArrary = firstPosList.split(" ");
                    var x = 0,
                        y = 0,
                        count = 0;
                    for (var ii = 0; ii < secondPosArrary.length; ii++) {
                        var thirdposItem = secondPosArrary[ii].split(',');
                        x += parseInt(thirdposItem[0]);
                        y += parseInt(thirdposItem[1]);
                        count++;
                    }
                    var unitpos1 = $('#floorplan-3-img').height() * 1.8 / 3506;
                    var unitpos2 = $('#floorplan-3-img').height() / 1958;
                    var leftPos = unitpos1 * x / count - 10;
                    var topPos = unitpos2 * y / count - 28;
                    $("#floorplan-position1").css({ left: leftPos + 'px', top: topPos + "px" });
                }
                $(".imp-shape-poly").click(function() {
                    var nowid = $(this).attr('id');
                    if (insteadid != '') {
                        $("#" + insteadid).css({ fill: 'rgba(0, 255, 0, 0.4)' });
                    }
                    $("#" + nowid).css({ fill: 'rgba(255, 255, 255, 0.4)' });
                    insteadid = nowid;
                    // getting booth of selected floor plan
                    var booth = nowid.substring(5);
                    $scope.showConfirm(booth);
                });
            });
        }
    ])

.controller('notificationsCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', '$ionicLoading', '$ionicScrollDelegate', '$ionicPopup', 'PostTypeService', 'ApiUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $rootScope, $stateParams, $timeout, $ionicLoading, $ionicScrollDelegate, $ionicPopup, PostTypeService, ApiUrl) {
        $scope.thereIsNoNotification = false;
        console.log('-----------Notifications list---------');

        $ionicLoading.show({
            template: 'Loading...'
        });
        $scope.wordLimit = 40;

        PostTypeService.getNotification(ApiUrl.notificationsUrl).then(function(response) {
            console.log(response);
            if (response !== false) {
                if (response.data !== []) {
                    $scope.thereIsNoNotification = false;
                    $scope.allNotifications = response.data;
                    $ionicLoading.hide();
                } else {
                    $scope.thereIsNoNotification = true;
                    $ionicLoading.hide();
                }
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('searchCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', '$ionicLoading', '$ionicScrollDelegate', 'PostTypeService', 'ApiUrl', 'BookmarkStorageSevice', 'MainVariableService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $rootScope, $stateParams, $timeout, $ionicLoading, $ionicScrollDelegate, PostTypeService, ApiUrl, BookmarkStorageSevice, MainVariableService) {
        $scope.dividerFunction = function(key) {
            return key;
        };
        // Remove auto dividers when we select another category.
        function removeElementsByClass(className) {
            $(".schedule-list .divider-element").remove();
            //$(".sponsors-list .divider-element-alph").remove();
            $(".exhibitors-list .divider-element-alph").remove();
            $(".speakers-list .divider-element-alph").remove();
        }

        var currentSearchData = [];
        // hide/show spioner:
        $scope.showSpiner = false;
        // Hide or show start icon.
        $scope.globalResult = true;
        $scope.showSearchMenu = true;
        $scope.showSearch = function() {
            $ionicScrollDelegate.scrollTop();
            if ($scope.showSearchMenu === false) {
                $scope.showSearchMenu = true;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $rootScope.$emit('change-divide-key', "");
                //$scope.alldata = [];
                $scope.searchInAllSpeakers = [];
                $scope.searchInAllSessions = [];
                $scope.searchInAllExhibitors = [];
            } else {
                $scope.globalResult = true;
                $scope.showSearchMenu = false;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alhp', "");
                $rootScope.$emit('change-divide-key', "");
                //$scope.alldata = [];
                $scope.searchInAllSpeakers = [];
                $scope.searchInAllSessions = [];
                $scope.searchInAllExhibitors = [];
            }
        };
        var allData = {
            speakers: [],
            sessions: [],
            exhibitors: []
        };
        // In this variable we will save all App data from WP db.
        var allDataFromDb = [];
        // Search word.
        $scope.search = {
            searchWord: ''
        };
        // Search functions:
        $scope.searchInfo = function(text) {
            $scope.showSpiner = true;
            $scope.globalResult = false;
            removeElementsByClass();
            $rootScope.$emit('change-divide-key-alph', "");
            $rootScope.$emit('change-divide-key', "");
            //$scope.alldata = [];
            $scope.searchInAllSpeakers = [];
            $scope.searchInAllSessions = [];
            $scope.searchInAllExhibitors = [];
            var searchText = text.toLowerCase();
            //MainVariableService.setSearchWord(searchText);
            //console.log(MainVariableService.getSearchWord());
            $timeout(function() { $scope.searchByTextField(searchText); }, 10);
        };
        $scope.searchByTextField = function(text) {
            //$scope.showSpiner = true;
            if (text !== '') {
                $scope.searchInAllSpeakers = allData.speakers.filter(function(item) {
                    if (item.title.rendered.toLowerCase().search(text) > -1) {
                        return true;
                    }
                    return false;
                });
                $scope.searchInAllSessions = allData.sessions.filter(function(item) {
                    if (item.title.rendered.toLowerCase().search(text) > -1) {
                        return true;
                    }
                    return false;
                });
                $scope.searchInAllExhibitors = allData.exhibitors.filter(function(item) {
                    if (item.title.rendered.toLowerCase().search(text) > -1) {
                        return true;
                    }
                    return false;
                });
            }
            $scope.showSpiner = false;
            // console.log($scope.searchInAllSpeakers);
            // console.log($scope.searchInAllSessions);
            // console.log($scope.searchInAllExhibitors);
            //currentSearchData = currentSearchData.concat($scope.searchInAllSpeakers);
            //currentSearchData = currentSearchData.concat($scope.searchInAllSessions);
            //currentSearchData = currentSearchData.concat($scope.searchInAllExhibitors);
            //console.log(currentSearchData);
            //MainVariableService.setSearchData(currentSearchData);
            //console.log(MainVariableService.getSearchData());
        };
        // Reset search form.
        $scope.resetSearch = function() {
            $scope.search.searchWord = '';
            removeElementsByClass();
            $rootScope.$emit('change-divide-key-alph', "");
            $rootScope.$emit('change-divide-key', "");
            //$scope.alldata = [];
            $scope.searchInAllSpeakers = [];
            $scope.searchInAllSessions = [];
            $scope.searchInAllExhibitors = [];
            $scope.globalResult = true;
        };

        var gotSessions = false;
        var gotSpeakers = false;
        var gotExhibitors = false;
        // Sessions:
        PostTypeService.getPosts(ApiUrl.sessionsUrl).then(function(responseSessions) {
            if (responseSessions !== false) {
                allData.sessions = responseSessions.data;
                gotSessions = true;
                //console.log(allData.sessions);
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
        // Speakers:
        var allSpeakersForSearch = [];
        PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 1).then(function(responseNextPage) {
            allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
            PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 2).then(function(responseNextPage) {
                allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 3).then(function(responseNextPage) {
                    allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                    PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 4).then(function(responseNextPage) {
                        allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                        var gotSpeakers = true;
                        allData.speakers = allSpeakersForSearch;
                        //console.log(allData.speakers);
                    });
                });
            });
        });
        // Exhibitors:
        PostTypeService.getPosts(ApiUrl.exhibitorsUrl).then(function(responseExhibitors) {
            var gotExhibitors = true;
            var dataPages = responseExhibitors.headers()["x-wp-totalpages"];
            allData.exhibitors = responseExhibitors.data;
            for (var index = 1; index < dataPages; index++) {
                var currentPageNumber = index += 1;
                PostTypeService.getAllPostsFromNextPage(ApiUrl.exhibitorsUrl, currentPageNumber).then(function(responseNextPage) {
                    allData.exhibitors = allData.exhibitors.concat(responseNextPage.data);
                    //console.log(allData.exhibitors)
                });
            }
        });

        $ionicLoading.show({
            template: 'Loading...'
        });
        $timeout(function() {
            $ionicLoading.hide();
        }, 1500);
        $timeout(function() {
            $ionicLoading.hide();
            if (gotSessions || gotSpeakers || gotExhibitors) {
                allDataFromDb = allDataFromDb.concat(allData.exhibitors);
                allDataFromDb = allDataFromDb.concat(allData.speakers);
                allDataFromDb = allDataFromDb.concat(allData.sessions);
                //console.log(allDataFromDb);
            }
        }, 4000);


        $scope.addBookmarkSpeaker = function(speakerObj) {
            //console.log('-----Add bookmark page----');
            var favoriteItems = BookmarkStorageSevice.getBookmarks("speakers");
            var isFavoriite = false;

            for (var index = 0; index < favoriteItems.length; index++) {
                if (speakerObj.id === favoriteItems[index].id) {
                    isFavoriite = true;
                }
            }
            if (isFavoriite) {
                $("#" + speakerObj.id).removeClass("ion-android-star");
                $("#" + speakerObj.id).addClass("ion-android-star-outline");
                BookmarkStorageSevice.removeBookmarks(speakerObj.id, "speakers");
            } else {
                $("#" + speakerObj.id).removeClass("ion-android-star-outline");
                $("#" + speakerObj.id).addClass("ion-android-star");
                BookmarkStorageSevice.bookmarkPost(speakerObj, "speakers");
            }
        };

        $scope.addBookmarkExhibitor = function(exhibitorObj) {
            //console.log('-----Add bookmark page----');
            var favoriteItems = BookmarkStorageSevice.getBookmarks("exhibitors");
            var isFavoriite = false;

            for (var index = 0; index < favoriteItems.length; index++) {
                if (exhibitorObj.id === favoriteItems[index].id) {
                    isFavoriite = true;
                }
            }
            if (isFavoriite) {
                $("#" + exhibitorObj.id).removeClass("ion-android-star");
                $("#" + exhibitorObj.id).addClass("ion-android-star-outline");
                BookmarkStorageSevice.removeBookmarks(exhibitorObj.id, "exhibitors");
            } else {
                $("#" + exhibitorObj.id).removeClass("ion-android-star-outline");
                $("#" + exhibitorObj.id).addClass("ion-android-star");
                BookmarkStorageSevice.bookmarkPost(exhibitorObj, "exhibitors");
            }
        };
    }
])

.controller('speakersCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', '$state', '$ionicLoading', '$ionicScrollDelegate', 'PostTypeService', 'ApiUrl', 'BookmarkStorageSevice', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $rootScope, $stateParams, $timeout, $state, $ionicLoading, $ionicScrollDelegate, PostTypeService, ApiUrl, BookmarkStorageSevice) {
        console.log('-----------Speaker list---------');
        $scope.dividerFunction = function(key) {
            return key;
        };
        PostTypeService.setPageCounter();
        // Remove auto dividers when we select another category.
        function removeElementsByClass(className) {
            $(".speakers-list .divider-element-alph").remove();
        }

        $scope.allSpeakers = [];
        // Show hide load more button.
        $scope.hideLoadMoreButton = false;
        // Show hide search menu.
        $scope.showSearchMenu = false;
        $scope.showSearch = function() {
            $ionicScrollDelegate.scrollTop();
            if ($scope.showSearchMenu === false) {
                $scope.showSearchMenu = true;
                $scope.hideLoadMoreButton = false;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allSpeakers = [];
            } else {
                $scope.showSearchMenu = false;
                $scope.hideLoadMoreButton = true;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alhp', "");
                $scope.allSpeakers = $scope.currentSpeakers;
            }
        };
        $scope.data = {
            searchWord: ''
        };
        $scope.showSpiner = false;
        $scope.loadMoreSpeakers = function() {
            //alert('Load Moreeee');
            // if($scope.allSpeakers.length > ) {

            // }
            $scope.showSpiner = true;
            PostTypeService.getNext10Posts(ApiUrl.speakerUrl).then(function(result) {
                if (result !== false) {
                    //console.log(result);
                    $scope.showSpiner = false;
                    var currentspeakers = $scope.allSpeakers;
                    $scope.allSpeakers = [];
                    removeElementsByClass();
                    $rootScope.$emit('change-divide-key-alph', "");
                    $timeout(function() {
                        $scope.allSpeakers = currentspeakers.concat(result);
                        $ionicScrollDelegate.scrollBottom(true);
                        console.log($scope.allSpeakers.length);
                    }, 10);
                } else {
                    PostTypeService.callBackError();
                    //console.log('Something went wrong!');
                }

            });
        };
        $ionicLoading.show({
            template: 'Loading...'
        });
        PostTypeService.getFirst10Posts(ApiUrl.speakerUrl).then(function(resultFirst10) {
            if (resultFirst10 !== false) {
                //console.log(resultFirst10);
                $ionicLoading.hide();
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allSpeakers = resultFirst10;
                $scope.currentSpeakers = resultFirst10;
                $scope.hideLoadMoreButton = true;
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });

        // Get all speakers from WP DB.
        var allSpeakersForSearch = [];
        PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 1).then(function(responseNextPage) {
            allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
            PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 2).then(function(responseNextPage) {
                allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 3).then(function(responseNextPage) {
                    allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                    PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 4).then(function(responseNextPage) {
                        allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                            //console.log(allSpeakersForSearch);


                        $scope.search = function(text) {
                            removeElementsByClass();
                            $rootScope.$emit('change-divide-key-alph', "");
                            $scope.allSpeakers = [];
                            var searchText = text.toLowerCase();
                            $timeout(function() { $scope.searchByTextField(searchText); }, 10);
                        };
                        $scope.searchByTextField = function(text) {
                            if (text !== '') {
                                $scope.allSpeakers = allSpeakersForSearch.filter(function(speaker) {
                                    if (speaker.title.rendered.toLowerCase().search(text) > -1) {
                                        return true;
                                    }
                                    return false;
                                });
                            }
                        };
                        // Reset search form.
                        $scope.resetSearch = function() {
                            $scope.data.searchWord = '';
                            removeElementsByClass();
                            $rootScope.$emit('change-divide-key-alph', "");
                            $scope.allSpeakers = [];
                        };


                        function bookmarkPost(post) {
                            $ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
                            PostService.bookmarkPost(post);
                        };

                        $scope.addFavorite = function() {
                            alert('add to favorite');
                            //bookmarkPost(post);
                        };

                        // must pass "post" from ng-repeat in the list page
                        $scope.addBookmark = function(speakerObj) {
                            console.log('-----Add bookmark page----');
                            var favoriteItems = BookmarkStorageSevice.getBookmarks("speakers");
                            console.log(favoriteItems)
                            var isFavoriite = false;

                            for (var index = 0; index < favoriteItems.length; index++) {
                                if (speakerObj.id === favoriteItems[index].id) {
                                    isFavoriite = true;
                                }
                            }
                            if (isFavoriite) {
                                $("#" + speakerObj.id).animateCss('jackInTheBox');
                                $("#" + speakerObj.id).removeClass("ion-android-star");
                                $("#" + speakerObj.id).addClass("ion-android-star-outline");
                                BookmarkStorageSevice.removeBookmarks(speakerObj.id, "speakers");
                            } else {
                                $("#" + speakerObj.id).animateCss('jackInTheBox');
                                $("#" + speakerObj.id).removeClass("ion-android-star-outline");
                                $("#" + speakerObj.id).addClass("ion-android-star");
                                BookmarkStorageSevice.bookmarkPost(speakerObj, "speakers");
                            }
                        };

                        // $scope.addBookmark = function(sessionObj) {
                        //     //console.log('-----Add bookmark page----');
                        //     //console.log(sessionObj);
                        //     BookmarkStorageSevice.bookmarkPost(sessionObj, "speakers");
                        // };
                    });
                });
            });
        });
    }
])

.controller('attendeesCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', '$state', '$ionicLoading', '$ionicScrollDelegate', 'PostTypeService', 'ApiUrl', 'BookmarkStorageSevice', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $rootScope, $stateParams, $timeout, $state, $ionicLoading, $ionicScrollDelegate, PostTypeService, ApiUrl, BookmarkStorageSevice) {
        console.log('-----------Speaker list---------');
        $scope.dividerFunction = function(key) {
            return key;
        };
        PostTypeService.setPageCounter();
        // Remove auto dividers when we select another category.
        function removeElementsByClass(className) {
            $(".speakers-list .divider-element-alph").remove();
        }

        $scope.allSpeakers = [];
        // Show hide load more button.
        $scope.hideLoadMoreButton = false;
        // Show hide search menu.
        $scope.showSearchMenu = false;
        $scope.showSearch = function() {
            $ionicScrollDelegate.scrollTop();
            if ($scope.showSearchMenu === false) {
                $scope.showSearchMenu = true;
                $scope.hideLoadMoreButton = false;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allSpeakers = [];
            } else {
                $scope.showSearchMenu = false;
                $scope.hideLoadMoreButton = true;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alhp', "");
                $scope.allSpeakers = $scope.currentSpeakers;
            }
        };
        $scope.data = {
            searchWord: ''
        };
        $scope.showSpiner = false;
        $scope.loadMoreSpeakers = function() {
            //alert('Load Moreeee');
            // if($scope.allSpeakers.length > ) {

            // }
            $scope.showSpiner = true;
            PostTypeService.getNext10Posts(ApiUrl.speakerUrl).then(function(result) {
                if (result !== false) {
                    //console.log(result);
                    $scope.showSpiner = false;
                    var currentspeakers = $scope.allSpeakers;
                    $scope.allSpeakers = [];
                    removeElementsByClass();
                    $rootScope.$emit('change-divide-key-alph', "");
                    $timeout(function() {
                        $scope.allSpeakers = currentspeakers.concat(result);
                        $ionicScrollDelegate.scrollBottom(true);
                        console.log($scope.allSpeakers.length);
                    }, 10);
                } else {
                    PostTypeService.callBackError();
                    //console.log('Something went wrong!');
                }

            });
        };
        $ionicLoading.show({
            template: 'Loading...'
        });
        PostTypeService.getFirst10Posts(ApiUrl.speakerUrl).then(function(resultFirst10) {
            if (resultFirst10 !== false) {
                //console.log(resultFirst10);
                $ionicLoading.hide();
                removeElementsByClass();
                $rootScope.$emit('change-divide-key-alph', "");
                $scope.allSpeakers = resultFirst10;
                $scope.currentSpeakers = resultFirst10;
                $scope.hideLoadMoreButton = true;
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });

        // Get all speakers from WP DB.
        var allSpeakersForSearch = [];
        PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 1).then(function(responseNextPage) {
            allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
            PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 2).then(function(responseNextPage) {
                allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 3).then(function(responseNextPage) {
                    allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                    PostTypeService.getAllPostsFromNextPage(ApiUrl.speakersUrl, 4).then(function(responseNextPage) {
                        allSpeakersForSearch = allSpeakersForSearch.concat(responseNextPage.data)
                            //console.log(allSpeakersForSearch);


                        $scope.search = function(text) {
                            removeElementsByClass();
                            $rootScope.$emit('change-divide-key-alph', "");
                            $scope.allSpeakers = [];
                            var searchText = text.toLowerCase();
                            $timeout(function() { $scope.searchByTextField(searchText); }, 10);
                        };
                        $scope.searchByTextField = function(text) {
                            if (text !== '') {
                                $scope.allSpeakers = allSpeakersForSearch.filter(function(speaker) {
                                    if (speaker.title.rendered.toLowerCase().search(text) > -1) {
                                        return true;
                                    }
                                    return false;
                                });
                            }
                        };
                        // Reset search form.
                        $scope.resetSearch = function() {
                            $scope.data.searchWord = '';
                            removeElementsByClass();
                            $rootScope.$emit('change-divide-key-alph', "");
                            $scope.allSpeakers = [];
                        };


                        function bookmarkPost(post) {
                            $ionicLoading.show({ template: 'Post Saved!', noBackdrop: true, duration: 1000 });
                            PostService.bookmarkPost(post);
                        };

                        $scope.addFavorite = function() {
                            alert('add to favorite');
                            //bookmarkPost(post);
                        };

                        // must pass "post" from ng-repeat in the list page
                        $scope.addBookmark = function(speakerObj) {
                            console.log('-----Add bookmark page----');
                            var favoriteItems = BookmarkStorageSevice.getBookmarks("speakers");
                            console.log(favoriteItems)
                            var isFavoriite = false;

                            for (var index = 0; index < favoriteItems.length; index++) {
                                if (speakerObj.id === favoriteItems[index].id) {
                                    isFavoriite = true;
                                }
                            }
                            if (isFavoriite) {
                                $("#" + speakerObj.id).animateCss('jackInTheBox');
                                $("#" + speakerObj.id).removeClass("ion-android-star");
                                $("#" + speakerObj.id).addClass("ion-android-star-outline");
                                BookmarkStorageSevice.removeBookmarks(speakerObj.id, "speakers");
                            } else {
                                $("#" + speakerObj.id).animateCss('jackInTheBox');
                                $("#" + speakerObj.id).removeClass("ion-android-star-outline");
                                $("#" + speakerObj.id).addClass("ion-android-star");
                                BookmarkStorageSevice.bookmarkPost(speakerObj, "speakers");
                            }
                        };

                        // $scope.addBookmark = function(sessionObj) {
                        //     //console.log('-----Add bookmark page----');
                        //     //console.log(sessionObj);
                        //     BookmarkStorageSevice.bookmarkPost(sessionObj, "speakers");
                        // };
                    });
                });
            });
        });
    }
])

.controller('speakerCtrl', ['$scope', '$stateParams', '$ionicLoading', 'PostTypeService', 'ApiUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, PostTypeService, ApiUrl) {
        // Current speaker ID.
        $scope.currentSpeakerId = $stateParams.id;
        $ionicLoading.show({
            template: 'Loading...'
        });
        // Get details for current user.
        PostTypeService.getPost(ApiUrl.speakerUrl, $scope.currentSpeakerId).then(function(result) {
            if (result !== false) {
                console.log('Single speaker');
                console.log(result);
                console.log(result.acf.slots);
                $ionicLoading.hide();
                $scope.singleSpeakerInfo = result;
            } else {
                PostTypeService.callBackErrorDetails();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('attendeeCtrl', ['$scope', '$stateParams', '$ionicLoading', 'PostTypeService', 'ApiUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, PostTypeService, ApiUrl) {
        // Current speaker ID.
        $scope.currentSpeakerId = $stateParams.id;
        $ionicLoading.show({
            template: 'Loading...'
        });
        // Get details for current user.
        PostTypeService.getPost(ApiUrl.speakerUrl, $scope.currentSpeakerId).then(function(result) {
            if (result !== false) {
                console.log('Single speaker');
                console.log(result);
                console.log(result.acf.slots);
                $ionicLoading.hide();
                $scope.singleSpeakerInfo = result;
            } else {
                PostTypeService.callBackErrorDetails();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('activityfeedCtrl', [
    '$scope',
    '$stateParams',
    '$ionicLoading',
    'PostTypeService',
    'ApiUrl',
    '$ionicModal',
    function(
        $scope,
        $stateParams,
        $ionicLoading,
        PostTypeService,
        ApiUrl,
        $ionicModal) {
        $ionicLoading.show({
            template: 'Loading...'
        });

        // Filter options(Modal) 
        $ionicModal.fromTemplateUrl('templates/comments.html', {
            id: 1,
            scope: $scope,
            animation: 'slide-in-up',
            backdropClickToClose: true
        }).then(function(modal) {
            $scope.commentsModal = modal;
        });

        $scope.openModal = function() {
            console.log('Open');
            $scope.commentsModal.show();
        };
        $scope.closeModal = function() {
            console.log('Close');
            $scope.commentsModal.hide();
        };



        $ionicLoading.hide();
    }
])

.controller('sponsorCtrl', ['$scope', '$stateParams', '$ionicLoading', '$cordovaInAppBrowser', 'PostTypeService', 'ApiUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, $cordovaInAppBrowser, PostTypeService, ApiUrl) {
        console.log('------------- Single sponsor -------------');
        // Current sponsor ID.
        $scope.currentSponsorId = $stateParams.id;
        var options = {
            location: 'no',
            clearcache: 'no',
            toolbar: 'yes'
        };
        $scope.openBrowser = function(exhibitorsLink) {
            var realUrl = '';
            if (exhibitorsLink.indexOf('http') > -1) {
                realUrl = exhibitorsLink;
            } else {
                realUrl = 'http://' + exhibitorsLink;
            }
            $cordovaInAppBrowser.open(realUrl, '_blank', options)
                .then(function(event) {
                    // success
                })
                .catch(function(event) {
                    // error
                });
        }
        $ionicLoading.show({
            template: 'Loading...'
        });
        PostTypeService.getPost(ApiUrl.exhibitorUrl, $scope.currentSponsorId).then(function(result) {
            if (result !== false) {
                $ionicLoading.hide();
                $scope.singleSponsor = {
                    name: result.title.rendered,
                    sponsorDescription: result.content.rendered,
                    website: result.acf.corporate_website,
                    booth: result.acf.booth,
                    email: result.acf["contact_e-mail"],
                    phone: result.acf.phone,
                    country: result.acf.country,
                    city: result.acf.city,
                    street: result.acf.street_adress
                };
                // image
                $scope.singleSponsor.logo = result.better_featured_image.source_url;
                // level
                var currentLevel = result.categories[0];
                if (currentLevel === 22) {
                    $scope.singleSponsor.level = "Platinum";
                } else if (currentLevel === 21) {
                    $scope.singleSponsor.level = "Gold";
                } else if (currentLevel === 20) {
                    $scope.singleSponsor.level = "Silver";
                } else if (currentLevel === 26) {
                    $scope.singleSponsor.level = "Bronze";
                }
            } else {
                PostTypeService.callBackErrorDetails();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('sessionCtrl', ['$scope', '$stateParams', '$ionicLoading', 'PostTypeService', 'ApiUrl', 'FormidableService', '$cordovaToast', '$timeout', 'BookmarkStorageSevice', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, PostTypeService, ApiUrl, FormidableService, $cordovaToast, $timeout, BookmarkStorageSevice) {
        $scope.showContainer = true;
        $scope.activeSection = true;
        $scope.formHtml = '';
        $scope.showerror = false;
        $scope.selectSection = function(index) {
            if (index) {
                $scope.showContainer = true;
                $scope.activeSection = true;
            } else {
                $scope.showContainer = false;
                $scope.activeSection = false;

                $timeout(function() {
                    document.getElementById("demo1").innerHTML = $scope.formHtml;
                    var actionurl = $("#form_2cqcx2").attr("action");
                    $("#field_session_id2").attr("value", $scope.currentSessionId);
                    $("#form_2cqcx2").attr("action", '');
                    $("#form_2cqcx2").attr("onsubmit", "return false;");
                    $("#form_2cqcx2").submit(function() {
                        $ionicLoading.show({
                            template: '<ion-spinner></ion-spinner>'
                        });
                        $.ajax({
                            type: 'POST',
                            url: actionurl,
                            data: $('#form_2cqcx2').serialize(),
                            success: function(response) {
                                console.log(response);
                                $ionicLoading.hide();
                                var index1 = response.indexOf('There was a problem');
                                var index2 = response.indexOf("successfully");
                                if (index2 > -1) {
                                    $("#form_2cqcx2")[0].reset();
                                    $scope.showerror = false;
                                    showToast('Your responses were successfully submitted. Thank you!');
                                } else {
                                    $scope.showerror = true;
                                    showToast('Please valid form');
                                }
                            },
                            error: function(err) {
                                console.log(err);
                                $ionicLoading.hide();
                                showToast('Failed');
                                // console.log("error",err);
                            }
                        });

                    });
                }, 10);

            }

        };
        $scope.currentSessionId = $stateParams.id;
        $scope.singleSessionInfo = {};
        $ionicLoading.show({
            template: 'Loading ...'
        });
        // Details for current user.
        PostTypeService.getPost(ApiUrl.sessionUrl, $scope.currentSessionId).then(function(result) {
            if (result !== false) {
                console.log('Single session');
                console.log(result);
                $ionicLoading.hide();
                var currentResult = result;
                for (var index = 0; index < currentResult.acf.slots.length; index++) {
                    currentResult.acf.slots[index].eventDay = currentResult.day[0];
                }
                $scope.singleSession = currentResult;
            } else {
                PostTypeService.callBackErrorDetails();
            }
        });

        FormidableService.getForm(ApiUrl.sessionFormHtml).then(function(result) {
            $scope.formHtml = result.renderedHtml;
            //console.log($scope.formHtml);
            // Append formidable Form on form App page.               
        });

        var showToast = function(text) {
                $cordovaToast.showLongBottom(text).then(function(success) {
                    // success
                }, function(error) {
                    // error
                });
            }
            //Bookmark Slots:
        $scope.addBookmarkSlot = function(slotObj) {
            console.log('-----Add bookmark slots page----');
            var favoriteItems = BookmarkStorageSevice.getBookmarks("slots");
            var isFavoriite = false;

            for (var index = 0; index < favoriteItems.length; index++) {
                if (slotObj.id === favoriteItems[index].id) {
                    isFavoriite = true;
                }
            }
            if (isFavoriite) {
                $("#" + slotObj.id).animateCss('jackInTheBox');
                $("#" + slotObj.id).removeClass("ion-android-star");
                $("#" + slotObj.id).addClass("ion-android-star-outline");
                BookmarkStorageSevice.removeBookmarks(slotObj.id, "slots");
            } else {
                $("#" + slotObj.id).animateCss('jackInTheBox');
                $("#" + slotObj.id).removeClass("ion-android-star-outline");
                $("#" + slotObj.id).addClass("ion-android-star");
                BookmarkStorageSevice.bookmarkPost(slotObj, "slots");
            }
        };

    }
])


.controller('exhibitorCtrl', ['$scope', '$stateParams', '$ionicLoading', '$cordovaInAppBrowser', 'PostTypeService', 'ApiUrl', '$state', '$ionicHistory', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, $cordovaInAppBrowser, PostTypeService, ApiUrl, $state, $ionicHistory) {
        console.log('Single exhibitor');
        $scope.hideImageBox = false;
        // Current exhibitor ID.
        $scope.currentExhibitorId = $stateParams.id;
        var options = {
            location: 'no',
            clearcache: 'no',
            toolbar: 'yes'
        };
        $scope.openBrowser = function(exhibitorsLink) {
            var realUrl = '';
            if (exhibitorsLink.indexOf('http') > -1) {
                realUrl = exhibitorsLink;
            } else {
                realUrl = 'http://' + exhibitorsLink;
            }
            $cordovaInAppBrowser.open(realUrl, '_blank', options)
                .then(function(event) {
                    // success
                })
                .catch(function(event) {
                    // error
                });
        }

        $scope.gotofloorpaln = function() {
            var boothList = $scope.singleExhibitor.booth.split(', ');
            // console.log("boothlist", boothList[0]);
            var realBooth = boothList[0];
            if ($scope.singleExhibitor.booth.indexOf('T') > -1 || $scope.singleExhibitor.booth.indexOf('O') > -1) {
                if ($scope.singleExhibitor.booth.indexOf('ITA') > -1) {
                    $state.go("menu.subfloorplan3", { id: realBooth });
                } else if ($scope.singleExhibitor.booth.indexOf('Tent') > -1) {
                    $state.go("menu.subfloorplan1", { id: realBooth });
                } else {
                    $state.go("menu.subfloorplan2", { id: realBooth });
                }

            } else if ($scope.singleExhibitor.booth.indexOf('NFF') > -1) {
                $state.go("menu.subfloorplan3", { id: realBooth });
            } else {
                $state.go("menu.subfloorplan1", { id: realBooth });
            }
        }

        $ionicLoading.show({
            template: 'Loading...'
        });
        // Details for current post.
        PostTypeService.getPost(ApiUrl.exhibitorUrl, $scope.currentExhibitorId).then(function(result) {
            if (result !== false) {
                console.log(result);
                $scope.hideImageBox = false;
                if (result.better_featured_image !== null) {
                    $scope.hideImageBox = true;
                }
                $ionicLoading.hide();
                $scope.singleExhibitor = {
                    name: result.title.rendered,
                    exhibitorDescription: result.content.rendered,
                    website: result.acf.corporate_website,
                    booth: result.acf.booth,
                    email: result.acf["contact_e-mail"],
                    phone: result.acf.phone,
                    country: result.acf.country,
                    city: result.acf.city,
                    street: result.acf.street_adress
                };
                if ($scope.hideImageBox) {
                    $scope.singleExhibitor.logo = result.better_featured_image.source_url;
                }
            } else {
                PostTypeService.callBackErrorDetails();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('welcomeMessageCtrl', ['$scope', '$stateParams', '$ionicLoading', 'pagesContentService', 'ApiUrl', 'PostTypeService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, pagesContentService, ApiUrl, PostTypeService) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        pagesContentService.getContent(ApiUrl.welcomeMessagePage).then(function(result) {
            if (result !== false) {
                $ionicLoading.hide();
                $scope.content = result.content.rendered;
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
    }
])



.controller('newsCtrl', ['$scope', '$stateParams', '$ionicLoading', '$timeout', 'pagesContentService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, $timeout, pagesContentService) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        $timeout(function() { $ionicLoading.hide(); }, 1000);
    }
])

.controller('surveyCtrl', ['$scope', '$stateParams', 'pagesContentService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, pagesContentService) {


    }
])

.controller('notificationDetailsCtrl', ['$scope', '$stateParams', '$ionicLoading', '$ionicPopup', 'PostTypeService', 'ApiUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, $ionicPopup, PostTypeService, ApiUrl) {
        console.log('Notification Details page');
        // Current notif ID.
        $scope.currentNotificationId = $stateParams.id;
        $ionicLoading.show({
            template: 'Loading...'
        });
        PostTypeService.getPost(ApiUrl.notificationUrl, $scope.currentNotificationId).then(function(result) {
            if (result !== false) {
                //console.log(result);
                $scope.notificationInfo = result;
                $ionicLoading.hide();
            } else {
                PostTypeService.callBackErrorDetails();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('videosCtrl', ['$scope', '$stateParams', 'Tutorials', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, Tutorials) {
        // Show / hide search menu.
        $scope.showSearchMenu = false;
        $scope.showSearch = function() {
            if ($scope.showSearchMenu === false) {
                $scope.showSearchMenu = true;
            } else {
                $scope.showSearchMenu = false;
            }
        };
        $scope.data = {
            search: ''
        };
        // Get all videos from file/Db.
        $scope.narrowed_tutorials = Tutorials.list;
        // Search by text field.
        $scope.search = function() {
            var searchWord = $scope.data.search.toLowerCase();
            if (searchWord === '') {
                $scope.narrowed_tutorials = Tutorials.list;
                return;
            }
            $scope.narrowed_tutorials = Tutorials.list.filter(function(tutorial) {
                if (tutorial.name.toLowerCase().indexOf(searchWord) > -1 || tutorial.description.toLowerCase().indexOf(searchWord) > -1) {
                    return true;
                }
                return false;
            });
        };

    }
])

.controller('bookmarksCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', 'BookmarkStorageSevice', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $rootScope, $stateParams, $timeout, BookmarkStorageSevice) {
        console.log('Bookmarks page');
        $scope.thereIsNoBookmarks = true;
        $scope.dividerFunction = function(key) {
            return key;
        };
        // Remove auto dividers when we select another category.
        function removeSpeaekrsElementsByClass(className) {
            $(".speakers-list .divider-element-alph").remove();
        }

        function removeExhibitorsElementsByClass(className) {
            $(".exhibitors-list .divider-element-alph").remove();
        }
        $scope.showSpiner = false;


        function loadMyBookmarks(callFrom) {
            $scope.showSpiner = true;
            $scope.bookmarkSpeakers = [];
            $scope.bookmarkExhibitors = [];
            $scope.bookmarkSpeakers = BookmarkStorageSevice.getBookmarks("speakers");
            $scope.bookmarkExhibitors = BookmarkStorageSevice.getBookmarks("exhibitors");
            if ($scope.bookmarkSpeakers.length || $scope.bookmarkExhibitors.length) {
                $scope.thereIsNoBookmarks = false;
            } else {
                $scope.thereIsNoBookmarks = true;
            }
            $scope.showSpiner = false;
            console.log($scope.bookmarkExhibitors);
        }
        loadMyBookmarks(true);

        $scope.removeSpeakersFromBookmark = function(id) {
            $scope.showSpiner = true;
            BookmarkStorageSevice.removeBookmarks(id, "speakers");
            loadMyBookmarks(false);
        };

        $scope.removeExhibitorsFromBookmark = function(id) {
            $scope.showSpiner = true;
            BookmarkStorageSevice.removeBookmarks(id, "exhibitors");
            loadMyBookmarks(false);
        };

    }
])

.controller('mySlotsCtrl', ['$scope', '$stateParams', '$timeout', 'BookmarkStorageSevice', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $timeout, BookmarkStorageSevice) {
        // the best would be if this data comes from API
        var daysId = [
            { 'id': 71, 'label': 'June 11, 2017' },
            { 'id': 72, 'label': 'June 12, 2017' },
            { 'id': 77, 'label': 'June 13, 2017' },
            { 'id': 78, 'label': 'June 14, 2017' }
        ];
        // All bookmarked slots for all days.
        var daysData = {};
        $scope.thereIsNoMySessions = true;

        function loadMySlotsPage() {
            var allSlots = BookmarkStorageSevice.getBookmarks("slots");
            console.log(allSlots);

            for (var index = 0; index < daysId.length; index++) {
                var currentDayId = daysId[index].id;
                daysData[currentDayId] = [];
                for (var indexSlot = 0; indexSlot < allSlots.length; indexSlot++) {
                    if (daysId[index].id === allSlots[indexSlot].eventDay) {
                        daysData[currentDayId].push(allSlots[indexSlot]);
                    }
                }
            }
            if (daysData[71].length === 0 && daysData[72].length === 0 && daysData[77].length === 0 && daysData[78].length === 0) {
                $scope.thereIsNoMySessions = true;
            } else {
                $scope.thereIsNoMySessions = false;
                $scope.slotsDay1 = daysData[71];
                $scope.slotsDay2 = daysData[72];
                $scope.slotsDay3 = daysData[77];
                $scope.slotsDay4 = daysData[78];
            }
        }
        // load the page.
        loadMySlotsPage();

        $scope.removeFromMySlots = function(id) {
            $scope.showSpiner = true;
            BookmarkStorageSevice.removeBookmarks(id, "slots");
            $timeout(function() { loadMySlotsPage(); }, 10);
        };

    }
])

.controller('galleryCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams) {


    }
])

.controller('videoCtrl', ['$scope', '$stateParams', 'Tutorials', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, Tutorials) {
        // Get current video.
        $scope.video = Tutorials.keys[$stateParams.videokey];

    }
])

.controller('abstractCtrl', ['$scope', '$stateParams', '$ionicLoading', 'PostTypeService', 'ApiUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, PostTypeService, ApiUrl) {
        // Current sessions ID.
        $scope.currentAbstratcId = $stateParams.id;
        //console.log($scope.currentAbstratcId);
        $scope.singleSessionInfo = {};
        $scope.thereISDetails = true;
        $ionicLoading.show({
            template: 'Loading...'
        });
        // Details for current user.
        PostTypeService.getAbstractDetails(ApiUrl.abstractUrl, $scope.currentAbstratcId).then(function(result) {
            //console.log('------------------------------Result-----------------------------------------------');
            if (result.status === 200) {
                console.log('---STATUS 200---');
                //console.log(result);
                $ionicLoading.hide();

                $scope.thereISDetails = true;
                $scope.singleAbstract = {
                    title: result.data.title.rendered,
                    description: result.data.content.rendered,
                };

            } else {
                console.log('---Status ERROR---');
                console.log(result);
                if (result.status === 404) {
                    console.log('Invalid post ID.');
                    $ionicLoading.hide();
                    $scope.thereISDetails = false;
                }
                if (result.status === -1) {
                    console.log('Something went wrong!');
                    //$ionicLoading.hide();
                    PostTypeService.callBackErrorDetails();
                    $scope.thereISDetails = false;
                } else {
                    $ionicLoading.hide();
                    $scope.thereISDetails = false;
                }
            }
        });
    }
])

.controller('googleMapCtrl', ['$scope', '$stateParams', '$ionicLoading', '$timeout', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, $timeout) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        $timeout(function() { $ionicLoading.hide(); }, 1000);
    }
])

.controller('wpLoginCtrl', ['$scope', '$stateParams', '$state', '$ionicLoading', 'AuthService', '$rootScope', 'UserInfoService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $state, $ionicLoading, AuthService, $rootScope, UserInfoService) {
        $scope.user = {};
        $scope.thereIsntUser = false;
        $scope.thereIsUser = false;

        $scope.doLogin = function() {
            $ionicLoading.show({
                template: 'Logging in...'
            });
            var user = {
                userName: $scope.user.userName,
                password: $scope.user.password
            };

            AuthService.doLogin(user)
                .then(function(user) {
                    //success
                    $scope.thereIsntUser = false;
                    $scope.thereIsUser = true;
                    $scope.success = 'Hi ' + user.data.username + ', welcome to our system!';
                    // $state.go('menu.wTC2017');
                    $state.go('menu.orders');
                    $ionicLoading.hide();
                }, function(err) {
                    console.log(err);
                    //err
                    $scope.thereIsntUser = true;
                    $scope.error = err;
                    $ionicLoading.hide();
                });
        };
    }
])

.controller('forgotPasswordCtrl', ['$scope', '$stateParams', '$state', '$ionicLoading', 'AuthService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $state, $ionicLoading, AuthService) {
        $scope.user = {};
        $scope.recoverPassword = function() {
            $ionicLoading.show({
                template: 'Recovering password...'
            });
            AuthService.doForgotPassword($scope.user.userName)
                .then(function(data) {
                    if (data.status == "error") {
                        $scope.error = data.error;
                    } else {
                        $scope.message = "Link for password reset has been emailed to you. Please check your email.";
                    }
                    $ionicLoading.hide();
                });
        };
    }
])

.controller('registrationCtrl', ['$scope', '$stateParams', '$state', '$ionicLoading', 'AuthService', '$rootScope', 'UserInfoService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $state, $ionicLoading, AuthService, $rootScope, UserInfoService) {
        $scope.user = {};
        $scope.doRegister = function() {
            $ionicLoading.show({
                template: 'Registering user...'
            });
            var user = {
                userName: $scope.user.userName,
                password: $scope.user.password,
                email: $scope.user.email,
                displayName: $scope.user.displayName
            };
            AuthService.doRegister(user)
                .then(function(user) {
                    //success
                    $state.go('menu.wTC2017');
                    $ionicLoading.hide();
                }, function(err) {
                    //err
                    $scope.error = err;
                    $ionicLoading.hide();
                });
        };
    }
])

.controller('formidableFormCtrl', ['$scope', '$stateParams', '$http', '$q', '$location', '$state', 'FormidableService', 'ApiUrl', '$ionicLoading', '$cordovaToast', 'PostTypeService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $http, $q, $location, $state, FormidableService, ApiUrl, $ionicLoading, $cordovaToast, PostTypeService) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        $scope.showerror = false;
        $scope.$on("$ionicView.enter", function() {
            FormidableService.getForm(ApiUrl.formidableFormHtml).then(function(result) {
                if (result !== false) {
                    $ionicLoading.hide();
                    console.log('---Get HTML--');
                    console.log(result);
                    $scope.formHtml = result.renderedHtml;
                    // console.log($scope.formHtml);
                    // Append formidable Form on form App page.
                    document.getElementById("demo").innerHTML = $scope.formHtml;
                    var actionurl = $("#form_2cqcx").attr("action");
                    $("#form_2cqcx").attr("action", '');
                    $("#form_2cqcx").attr("onsubmit", "return false;");
                    $("#form_2cqcx").submit(function() {
                        $ionicLoading.show({
                            template: '<ion-spinner></ion-spinner>'
                        });
                        $.ajax({
                            type: 'POST',
                            url: actionurl,
                            data: $('#form_2cqcx').serialize(),
                            success: function(response) {
                                console.log(response);
                                $ionicLoading.hide();
                                var index1 = response.indexOf('There was a problem');
                                var index2 = response.indexOf("successfully");
                                if (index2 > -1) {
                                    $("#form_2cqcx")[0].reset();
                                    $scope.showerror = false;
                                    showToast('Your responses were successfully submitted. Thank you!');
                                } else {
                                    $scope.showerror = true;
                                    showToast('Please valid form');
                                }
                            },
                            error: function(err) {
                                console.log(err);
                                $ionicLoading.hide();
                                showToast('Failed');
                                // console.log("error",err);
                            }
                        });
                    });
                } else {
                    PostTypeService.callBackError();
                    console.log('Something went wrong!');
                }
            });
        });

        var showToast = function(text) {
            $cordovaToast.showLongBottom(text).then(function(success) {
                // success
            }, function(error) {
                // error
            });
        }
    }
])

.controller('exhibitorsMapCtrl', ['$scope', '$stateParams', '$state', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $state) {
        $scope.list = [{ id: 1, title: 'Under Ground: Main Exhibition Hall' },
            { id: 2, title: 'External Hall' },
            { id: 3, title: 'Secretariat Media Area' }
        ];

        $scope.gotoFloorplan = function(index) {
            $state.go("menu.subfloorplan" + index);
        }

    }
])

.controller('iTAScheduleCtrl', ['$scope', '$stateParams', '$timeout', '$ionicLoading', '$cordovaInAppBrowser', '$ionicModal', 'pagesContentService', 'ApiUrl', 'PostTypeService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $timeout, $ionicLoading, $cordovaInAppBrowser, $ionicModal, pagesContentService, ApiUrl, PostTypeService) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        var options = {
            location: 'no',
            clearcache: 'no',
            toolbar: 'yes'
        };
        $scope.openPdf = function(link) {
            console.log(link);
            var realUrl = '';
            if (link.indexOf('http') > -1) {
                realUrl = link;
            } else {
                realUrl = 'http://' + link;
            }
            $cordovaInAppBrowser.open(encodeURI(realUrl), '_blank', options)
                .then(function(event) {
                    // success
                })
                .catch(function(event) {
                    // error
                });
        };
        pagesContentService.getContent(ApiUrl.detailedScheduleModalPage).then(function(result) {
            if (result !== false) {
                $scope.detailedScheduleModalContent = result.content.rendered;
            } else {
                //PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
        pagesContentService.getContent(ApiUrl.itaScheduleImagePage).then(function(result) {
            if (result !== false) {
                $scope.itaScheduleImage = result.content.rendered;
            } else {
                //PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });

        pagesContentService.getContent(ApiUrl.itaSchedulePage).then(function(result) {
            if (result !== false) {
                $ionicLoading.hide();
                $scope.content = result.content.rendered;

                $ionicModal.fromTemplateUrl('templates/itaScheduleImage.html', {
                    id: 1,
                    scope: $scope,
                    backdropClickToClose: false
                }).then(function(modal) {
                    $scope.modal1 = modal;
                });
                $ionicModal.fromTemplateUrl('templates/detailedScheduleModal.html', {
                    id: 2,
                    scope: $scope,
                    backdropClickToClose: false
                }).then(function(modal) {
                    $scope.modal2 = modal;
                });

            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('myScheduleCtrl', ['$scope', '$rootScope', '$stateParams', '$timeout', 'pagesContentService', 'BookmarkStorageSevice', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $rootScope, $stateParams, $timeout, pagesContentService, BookmarkStorageSevice) {
        console.log('My schedule page');
        $scope.dividerFunction = function(key) {
            return key;
        };
        // Remove auto dividers when we select another category.
        function removeElementsByClass(className) {
            $(".schedule-list .divider-element").remove();
        }

        // All days taxonomies.
        $scope.days = [
            { 'id': 71, 'label': 'June 11, 2017' },
            { 'id': 72, 'label': 'June 12, 2017' },
            { 'id': 77, 'label': 'June 13, 2017' },
            { 'id': 78, 'label': 'June 14, 2017' }
            // { 'id': 77, 'label': 'June 15, 2017' },
            // { 'id': 77, 'label': 'June 16, 2017' }
        ];
        $scope.data = {
            day: $scope.days[0].id
        };


        $scope.thereIsNoMySessions = false;
        $scope.showSpiner = false;
        $scope.bookmarkSessionsForCurrentDay = false;

        function loadMySchedule(dayId) {
            // removeElementsByClass();
            // $rootScope.$emit('change-divide-key-alph', "");
            // $scope.bookmarkSessions = [];
            $scope.showSpiner = true;
            //$scope.bookmarkSessions = BookmarkStorageSevice.getBookmarks("schedule");
            var currentBookmarkSessions = BookmarkStorageSevice.getBookmarks("schedule");
            $scope.bookmarkSessions = currentBookmarkSessions.filter(function(session) {
                if (session.day[0] === dayId) {
                    return true;
                }
                return false;
            });


            // console.log('---------------------------');
            console.log($scope.bookmarkSessions);
            // console.log($scope.thereIsNoMySessions);
            if (currentBookmarkSessions.length) {
                $scope.thereIsNoMySessions = false;
            } else {
                $scope.thereIsNoMySessions = true;
            }
            if ($scope.bookmarkSessions.length) {
                $scope.bookmarkSessionsForCurrentDay = false;
            } else {
                $scope.bookmarkSessionsForCurrentDay = true;
            }
            $scope.showSpiner = false;
        }

        loadMySchedule($scope.days[0].id);

        $scope.removeFromMySchedule = function(id) {
            $scope.showSpiner = true;
            BookmarkStorageSevice.removeBookmarks(id, "schedule");
            $scope.bookmarkSessions = [];
            $scope.thereIsNoMySessions = false;
            removeElementsByClass();
            $rootScope.$emit('change-divide-key', "");
            $timeout(function() { loadMySchedule($scope.data.day); }, 10);
        };

        // Logic about days slider.
        $scope.activeSection = 1;
        $scope.changeSection = function(sectionNumber, dayId) {
            if (dayId !== $scope.data.day) {
                $scope.activeSection = sectionNumber;
                $scope.data.day = dayId;
                removeElementsByClass();
                $rootScope.$emit('change-divide-key', "");
                loadMySchedule(dayId);
            }
        };
    }
])

.controller('programOverviewCtrl', ['$scope', '$stateParams', '$ionicLoading', 'pagesContentService', 'ApiUrl', 'PostTypeService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, pagesContentService, ApiUrl, PostTypeService) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        pagesContentService.getContent(ApiUrl.programOverview).then(function(result) {
            if (result !== false) {
                $ionicLoading.hide();
                $scope.content = result.content.rendered;
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('generalInformationCtrl', ['$scope', '$stateParams', '$ionicLoading', 'pagesContentService', 'ApiUrl', 'PostTypeService', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
    // You can include any angular dependencies as parameters for this function
    // TIP: Access Route Parameters for your page via $stateParams.parameterName
    function($scope, $stateParams, $ionicLoading, pagesContentService, ApiUrl, PostTypeService) {
        $ionicLoading.show({
            template: 'Loading...'
        });
        pagesContentService.getContent(ApiUrl.generalInfoPage).then(function(result) {
            if (result !== false) {
                $ionicLoading.hide();
                $scope.content = result.content.rendered;
            } else {
                PostTypeService.callBackError();
                //console.log('Something went wrong!');
            }
        });
    }
])

.controller('ProductsCtrl', ['$scope', '$rootScope', '$state', 'ProductsData', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $rootScope, $state, ProductsData) {
            $scope.title = 'Products';
            $scope.products = [];
            $scope.productPage = 0;
            ProductsData.clear();

            $scope.hasMoreProducts = function() {
                return (ProductsData.hasMore() || $scope.productPage == 0);
            };
            $scope.loadMoreProducts = function() {
                $rootScope.$broadcast('loading:show');
                ProductsData.getProductsAsync($scope.productPage + 1).then(
                    // successCallback
                    function() {
                        $scope.productPage++;
                        $scope.products = ProductsData.getAll();
                        $scope.$broadcast('scroll.infiniteScrollComplete');

                        $rootScope.$broadcast('loading:hide');
                    },
                    // errorCallback
                    function() {
                        $rootScope.$broadcast('loading:hide');
                        console.warn("Was unable to fetch products data from the API.");
                    }
                );
            };
            // Load once as soon as the page loads
            $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams) {
                //if (toState['name'] === 'app.products' && $scope.products.length == 0)
                //$scope.loadMoreProducts();
            });
        }
    ])
    .controller('ProductCtrl', ['$scope', '$stateParams', '$state', '$rootScope', '$ionicSlideBoxDelegate', 'ProductsData', 'BasketData', 'ReviewsData', 'CategoriesData', '$cordovaInAppBrowser', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $stateParams, $state, $rootScope, $ionicSlideBoxDelegate, ProductsData, BasketData, ReviewsData, CategoriesData, $cordovaInAppBrowser) {
            $rootScope.$broadcast('loading:show');
            console.log('$stateParams.product_id', $stateParams.product_id);
            // Try to get product locally, fallback to REST API
            ProductsData.getProductAsync($stateParams.product_id).then(function(product) {
                $scope.product = product;
                $scope.quantity = {
                    value: 1
                };
                // Required for the image gallery to update
                $ionicSlideBoxDelegate.update();
                $rootScope.$broadcast('loading:hide');
            });
            // Review loader (Despite the ambigious wording, review pagination is not supported by the WC API)
            ReviewsData.async($stateParams.product_id).then(function() {
                $scope.reviews = ReviewsData.getAll();
            });
            // Pretty print dates, e.g. "5 days ago"
            $scope.humaneDate = humaneDate;
            // In app browser
            $scope.openLink = function(url) {
                var options = {
                    location: 'no',
                    clearcache: 'yes',
                    toolbar: 'yes'
                };
                $cordovaInAppBrowser.open(url, '_blank', options)
                    .then(function(event) {
                        // success
                    })
                    .catch(function(event) {
                        // error
                    });
                // $cordovaInAppBrowser.open(url, '_blank');
            };
            // Add product to basket
            $scope.toBasket = function() {
                $scope.product['quantity'] = $scope.quantity.value;
                $scope.cartProduct = {};
                $scope.cartProduct = $scope.product;
                $scope.cartProduct['quantity'] = $scope.quantity.value;
                $scope.cartProduct['variation'] = $scope.product.variation;

                BasketData.add($scope.cartProduct);
            };
            $scope.isNumber = angular.isNumber;

            CategoriesData.async();

            $scope.getSlugByName = function(name) {
                return CategoriesData.getByName(name).slug;
            }
        }
    ])
    .controller('BasketCtrl', ['$scope', '$rootScope', '$state', '$sce', 'BasketData', 'MetaData', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $rootScope, $state, $sce, BasketData, MetaData) {
            $scope.meta = {};
            MetaData.getProperties().then(
                function(result) {
                    $scope.meta = result.meta;
                }
            );
            // Get the basket products
            $scope.basketProducts = BasketData.getBasket();
            // Calculate total price
            $scope.calculateTotal = function() {
                if ($scope.basketProducts.length > 0) {
                    var total_price = BasketData.getTotal();

                    if ($scope.meta.currency_position == 'left') {
                        $scope.totalPriceHtml = '<span class="amount">' +
                            $scope.meta.currency_format + total_price.toFixed(2) + '</span>';
                    } else if ($scope.meta.currency_position == 'left_space') {
                        $scope.totalPriceHtml = '<span class="amount">' +
                            $scope.meta.currency_format + ' ' + total_price.toFixed(2) + '</span>';
                    } else if ($scope.meta.currency_position == 'right') {
                        $scope.totalPriceHtml = '<span class="amount">' +
                            total_price.toFixed(2) + $scope.meta.currency_format + '</span>';
                    } else {
                        $scope.totalPriceHtml = '<span class="amount">' +
                            total_price.toFixed(2) + ' ' + $scope.meta.currency_format + '</span>';
                    }
                    $scope.totalPrice = total_price.toFixed(2);
                } else {
                    $scope.totalPriceHtml = '<span class="amount">0</span>';
                    $scope.totalPrice = 0;
                }
                return $scope.totalPriceHtml;
            }
            $scope.getFormmatedPrice = function(price) {
                var formmatedPrice;
                if ($scope.meta.currency_position == 'left') {
                    formmatedPrice = '<span class="amount">' +
                        $scope.meta.currency_format + price + '</span>';
                } else if ($scope.meta.currency_position == 'left_space') {
                    formmatedPrice = '<span class="amount">' +
                        $scope.meta.currency_format + ' ' + price + '</span>';
                } else if ($scope.meta.currency_position == 'right') {
                    formmatedPrice = '<span class="amount">' +
                        price + $scope.meta.currency_format + '</span>';
                } else {
                    formmatedPrice = '<span class="amount">' +
                        price + ' ' + $scope.meta.currency_format + '</span>';
                }
                return formmatedPrice;
            };
            $scope.emptyBasket = function() {
                $scope.basketProducts = [];
                BasketData.emptyBasket();
            };
            $scope.removeProduct = function(id) {
                var product = _.find($scope.basketProducts, { id: parseInt(id) });
                $scope.basketProducts.splice($scope.basketProducts.indexOf(product), 1);
                $rootScope.$broadcast('basket');
            };
            $scope.proceedToOrder = function() {
                $state.go('menu.payment');
            };
        }
    ])

.controller('PaymentCtrl', ['$scope', '$rootScope', '$ionicModal', '$state', 'UserData', 'BasketData', 'MetaData', 'CONFIG', 'PAYMENT_CONFIG', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $rootScope, $ionicModal, $state, UserData, BasketData, MetaData, CONFIG, PAYMENT_CONFIG) {
            $scope.meta = {};
            MetaData.getProperties().then(
                function(result) {
                    $scope.meta = result.meta;
                }
            );
            // Get the basket products
            $scope.basketProducts = BasketData.getBasket();
            $scope.paid = false;

            // Email validation regex with a simplified RFC 2822 implementation
            // which doesn't support double quotes and square brackets.
            var email_regex = RegExp(["[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*",
                "+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]",
                "(?:[a-z0-9-]*[a-z0-9])?"
            ].join(''));

            $scope.email = {
                addr: ''
            };

            // Register User
            $scope.registerUser = function() {
                // var url = CONFIG.site_url + "/wp-login.php?action=register";
                // window.open(url, '_blank');
                $state.go('menu.newCustomer');
            };

            $scope.evaluateEmail = function() {
                var valid = email_regex.test($scope.email.addr);

                if (valid) {
                    UserData.check($scope.email.addr).then(function(user) {
                        $scope.emailVerified = true;
                        $scope.user = user;
                        console.log(user);
                    }, function() {
                        $scope.emailVerified = false;
                    });
                } else {
                    $scope.emailVerified = false;
                    $scope.user = null;
                }
            };

            $scope.canPay = function() {
                $scope.totalPrice = BasketData.getTotal();
                return $scope.totalPrice > 0 && $scope.emailVerified;
            }

            $scope.payViaSite = function(method, paid) {
                // Account exists/user checks out, send order.
                BasketData.sendOrder(method, paid).then(
                    function(response) {
                        // Redirect to the account page of the user to finalize the order.
                        var url = CONFIG.site_url + "/index.php/my-account/";
                        // Redirect to payment page immediately
                        //var url = CONFIG.site_url + "/index.php/checkout/order-pay/" + response.data.order.order_number
                        //          + '/?pay_for_order=true&key=' + response.data.order.order_key;
                        window.open(url, '_blank');

                        $scope.basketProducts = [];
                        BasketData.emptyBasket();
                        $state.go('menu.products');
                    },
                    function(response) {
                        console.error("Error: order request could not be sent.", response);
                    });
            };

            // PayPalMobile implementation is not functional in browsers and will return an error.
            $scope.payViaPaypal = function() {

                // Billing info check
                var sa = $scope.user.customer.shipping_address;
                if (sa.address_1 === "" || sa.address_2 === "" ||
                    sa.city === "" || sa.company === "" || sa.country === "" ||
                    sa.first_name === "" || sa.last_name === "" ||
                    sa.postcode === "" || sa.state === "") {

                    $scope.shipping_address = sa;
                    $scope.shippingAddressModal.show();
                    return; // Once the modal is closed this function is called again.
                }

                var paymentDetails = new PayPalPaymentDetails(
                    // subtotal - Sub-total (amount) of items being paid for.
                    String($scope.totalPrice),
                    // shipping - Amount charged for shipping.
                    "0.00",
                    // tax - Amount charged for tax.
                    "0.00"
                );

                var payment = new PayPalPayment(
                    String($scope.totalPrice), // amount
                    $scope.store.meta.currency,
                    $scope.store.name + " mobile payment", // description of the payment
                    "Sale", // Sale (immediate payment) or Auth (authorization only)
                    paymentDetails // The details set above, the amount should be the sum of the details.
                );

                // Render payment UI
                window.PayPalMobile.renderSinglePaymentUI(
                    payment,
                    function(payment) {
                        $scope.completePayment();
                    },
                    function(error) {
                        console.warn(JSON.stringify(error));
                    }
                );
            };

            // Hide modal and go back to cart
            $scope.closeModal = function() {
                $scope.shippingAddressModal.hide();
                $state.go('menu.payment');
            };


            // Modal for shipping info
            $ionicModal.fromTemplateUrl('templates/shipping-info-modal.html', function(modal) {
                $scope.shippingAddressModal = modal; // Billing info check
            }, {
                scope: $scope,
                animation: 'slide-in-up'
            });

            // Shipping Info Modal Done button
            $scope.shippingModalAccept = function() {
                $scope.shippingAddressModal.hide();
                $scope.user.customer.shipping_address = $scope.shipping_address;
                $scope.payViaPaypal();
            };

            $scope.completePayment = function() {
                $scope.paid = true;
                // Maybe a more robust way of handling this is necessary
                BasketData.sendOrder('paypal', true).then(
                    function(response) {
                        console.log("payment complete: " + JSON.stringify(payment));
                    },
                    function(response) {
                        console.error("Error: order request could not be sent.", response);
                    });
                $scope.basketProducts = [];
                BasketData.emptyBasket();
            };


            $scope.isPaid = function() {
                return $scope.paid;
            };

            $ionicModal.fromTemplateUrl('templates/stripe-modal.html', function(modal) {
                $scope.stripeModal = modal;
            }, {
                scope: $scope,
                animation: 'slide-in-up'
            });

            /**
                Payment with stripe is a 2 step process, first you authenticate the user and
                generate a token, then you use that token to finalize the payment.
                The second step should be implemented on the server side for security reasons
                and it is out of the scope of this app.

                As such only a partial implementation
                will be shown along with guidelines for further steps.

                For more info about the process and supported configurations:
                    https://stripe.com/docs/checkout#integration-custom

                How to test:
                    https://stripe.com/docs/testing

                Supported countries and currencies:
                    https://support.stripe.com/questions/which-currencies-does-stripe-support

                Server side implementation:
                    https://stripe.com/docs/charges

            **/
            $scope.payViaStripe = function() {
                console.log("Stripe modal called.");

                $scope.stripeModal.show();

                /*
                var handler = StripeCheckout.configure({
                    key: PAYMENT_CONFIG.stripe.public_key,
                    image: 'images/woocommerce-logo.png',
                    locale: 'auto',
                    currency: 'EUR',
                    token: function(token) {
                        console.log("Stripe checked out", token);

                        // Use the token to create the charge with a server-side script.
                        // You can access the token ID with `token.id`
                        $scope.paid = true;
                    }
                });

                // Open Checkout with further options
                handler.open({
                    name: 'Stripe.com',
                    description: '2 widgets',
                    amount: 100 // In cents
                });
                */
            };

            $scope.getFormmatedPrice = function(price) {

                var formmatedPrice;

                if ($scope.meta.currency_position == 'left') {
                    formmatedPrice = '<span class="amount">' +
                        $scope.meta.currency_format + price + '</span>';
                } else if ($scope.meta.currency_position == 'left_space') {
                    formmatedPrice = '<span class="amount">' +
                        $scope.meta.currency_format + ' ' + price + '</span>';
                } else if ($scope.meta.currency_position == 'right') {
                    formmatedPrice = '<span class="amount">' +
                        price + $scope.meta.currency_format + '</span>';
                } else {
                    formmatedPrice = '<span class="amount">' +
                        price + ' ' + $scope.meta.currency_format + '</span>';
                }

                return formmatedPrice;
            };

        }
    ])
    .controller('NewCustomerCtrl', ['$scope', '$ionicPopup', 'UserData', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $ionicPopup, UserData) {
            $scope.customer = {};
            $scope.billing_address = {};
            $scope.shipping_address = {};
            $scope.customer.edit = true;

            $scope.createCustomer = function() {

                var data = {
                    customer: {
                        email: $scope.customer.email,
                        first_name: $scope.customer.first_name,
                        last_name: $scope.customer.last_name,
                        username: $scope.customer.username,
                        password: $scope.customer.password,
                        billing_address: {
                            first_name: $scope.billing_address.first_name,
                            last_name: $scope.billing_address.last_name,
                            company: $scope.billing_address.company,
                            address_1: $scope.billing_address.address_1,
                            address_2: $scope.billing_address.address_2,
                            city: $scope.billing_address.city,
                            state: $scope.billing_address.state,
                            postcode: $scope.billing_address.postcode,
                            country: $scope.billing_address.country,
                            email: $scope.billing_address.email,
                            phone: $scope.billing_address.phone
                        },
                        shipping_address: {
                            first_name: $scope.shipping_address.first_name,
                            last_name: $scope.shipping_address.last_name,
                            company: $scope.shipping_address.company,
                            address_1: $scope.shipping_address.address_1,
                            address_2: $scope.shipping_address.address_2,
                            city: $scope.shipping_address.city,
                            state: $scope.shipping_address.state,
                            postcode: $scope.shipping_address.postcode,
                            country: $scope.shipping_address.country
                        }
                    }
                };
                UserData.createCustomer(data).then(function(result) {
                    $scope.customer = result.data.customer;
                    $scope.customer.edit = false;

                    var alertPopup = $ionicPopup.alert({
                        title: 'Success',
                        template: 'Customer created successfully'
                    });

                }, function(result) {
                    console.log(result)
                    var alertPopup = $ionicPopup.alert({
                        title: 'Registration Error',
                        template: result.data.errors[0].message
                    });
                });
            }
        }
    ])
    .controller('OrdersCtrl', ['$scope', '$rootScope', '$stateParams', '$state', '$ionicLoading', 'UserData', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $rootScope, $stateParams, $state, $ionicLoading, UserData) {
            // $rootScope.$broadcast('loading:show');
            $ionicLoading.show({
                template: 'Loading...'
            });
            $scope.thereIsNoMyOrders = false;
            $scope.$on("$ionicView.enter", function() {
                var user = JSON.parse(window.localStorage.ionWordpress_user);
                UserData.getOrdersAsync(user.data.id).then(
                    function(result) {
                        if (result) {
                            $scope.orders = UserData.getOrders();
                            if ($scope.orders.orders.length) {
                                $scope.thereIsNoMyOrders = false;
                                // console.log('User Orders:');
                                // console.log($scope.orders.orders);
                            } else {
                                $scope.thereIsNoMyOrders = true;
                            }
                            // $rootScope.$broadcast('loading:hide');
                            $ionicLoading.hide();
                        } else {
                            $ionicLoading.hide();
                        }
                    }
                );
            });
            $scope.showOrder = function(orderid) {
                $state.go("menu.orderdetail", { order_id: orderid });
            }
        }
    ])
    .controller('OrderdetailCtrl', ['$scope', '$rootScope', '$stateParams', '$ionicLoading', 'UserData', 'ApiUrl', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function($scope, $rootScope, $stateParams, $ionicLoading, UserData, ApiUrl) {
            // $rootScope.$broadcast('loading:show');
            $ionicLoading.show({
                template: 'Loading...'
            });
            $scope.orderdetail = {};
            UserData.getOrderAsync($stateParams.order_id).then(
                function(result) {
                    $scope.orderdetail = result.data.order;
                    $scope.qrCode = ApiUrl.createQRCode[0] + $scope.orderdetail.id;
                    console.log($scope.orderdetail);
                    // $rootScope.$broadcast('loading:hide');
                    $ionicLoading.hide();
                },
                function(result) {
                    $ionicLoading.hide();
                }
            );
        }
    ])

.controller('myAccountCtrl', ['$scope', '$stateParams', 'UserData',
    function($scope, $stateParams, UserData) {
        var email_regex = RegExp(["[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*",
            "+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9]",
            "(?:[a-z0-9-]*[a-z0-9])?"
        ].join(''));
        $scope.email = {
            addr: ''
        };
        // Register User
        $scope.registerUser = function() {
            // var url = CONFIG.site_url + "/wp-login.php?action=register";
            // window.open(url, '_blank');
            $state.go('menu.newCustomer');
        };
        $scope.evaluateEmail = function() {
            var valid = email_regex.test($scope.email.addr);
            console.log('------------------');
            console.log(valid);
            if (valid) {
                UserData.check($scope.email.addr).then(function(user) {
                    $scope.emailVerified = true;
                    $scope.user = user;
                    console.log('--true--');
                    console.log(user);
                }, function(error) {
                    console.log('--false--');
                    console.log(error);
                    $scope.emailVerified = false;
                });
            } else {
                console.log('---fail---');
                $scope.emailVerified = false;
                $scope.user = null;
            }
        };
    }
])