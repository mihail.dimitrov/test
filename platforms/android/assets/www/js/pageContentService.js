angular.module('PageContentService', [])

.factory('pagesContentService', ['$q', '$http', 'ApiUrl', function($q, $http, ApiUrl) {
    // With this function we get all formidable forms or ant other information- depens on the API url.
    function getContent(pageId) {
        var url = ApiUrl.sitePagesLink + pageId;
        var deferred = $q.defer();
        $http.get(url).then(function(response) {
            deferred.resolve(response.data);
        }, function(response) {
            deferred.resolve(false);
        });
        return deferred.promise;
    }

    function getAllAPILinks() {
        // Page link:
        var url = "https://13367.de/wp-json/wp/v2/pages/1657";
        var deferred = $q.defer();
        $http.get(url).then(function(response) {
            deferred.resolve(response.data);
        }, function(response) {
            deferred.resolve(false);
        });
        return deferred.promise;
    }

    // Return this object and pass service functionalities to controller.
    var ret = {
        getContent: getContent,
        getAllAPILinks: getAllAPILinks
    };
    // Return this variable and use in controller.
    return ret;
}]);