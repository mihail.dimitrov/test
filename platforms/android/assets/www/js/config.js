angular.module("app").constant("CONFIG", {

    // The url of your domain, both HTTP and HTTPS are supported.
    site_url: 'https://firstunited-events.com/wtc2017',
    //site_url: 'https://13367.de',
    // site_url: 'http://localhost:8888/mobile/woocommerce-api/wordpress',

    // Max period of time to wait for reply from the server, defined in milliseconds.
    request_timeout: 6000,

    // The url that follows your main domain, the API version is of interest here, v3 is the latest.
    wc_api_endpoint: '/wc-api/v3',

    // Signature method
    //oauth_signature_method: 'HMAC-SHA256',
    oauth_signature_method: 'HMAC-SHA1',

    // Pair of credentials from your woocommerce installation, please refer to the documentation.
    // Apps today
    // wc_consumer_key: 'ck_719f1ed494f9b921243946588ab1d5ebf265b375',
    // wc_consumer_secret: 'cs_3071609afa31cab551a0b7286b98bc31a60c4c37',
    // FU
    wc_consumer_key: 'ck_d2b4d56f741858bbfb62fd2bf8dfdcb2a2a2a9af',
    wc_consumer_secret: 'cs_02c825662accf6ecd6ebe6a2f79ffc70450e411a',
    // 13367
    // wc_consumer_key: 'ck_a6ab87f1407907b4fdba2837960707819b916520',
    // wc_consumer_secret: 'cs_370c6b9bcff3cd87a8f819977b12f65133e9446d',
    // Localhost
    // wc_consumer_key: 'ck_86a13f2c0ec34a4c99374e120dba436d31f069c3',
    // wc_consumer_secret: 'cs_a0bc69e05eced4a8f464c08849c8f543946448d9',

    // The number of products to be fetched with each API call.
    products_per_page: 6,

    // The number of reviews to be fetched with each API call.
    reviews_per_page: 6,

    // Frontpage Slideshow Category
    slideshow_category: 'hoodies'

});