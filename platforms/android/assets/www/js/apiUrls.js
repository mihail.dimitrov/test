angular.module('ApiUrlModule', [])
    // In this module we will keep our API urls.
    // Just add your new api url in ret (Object) and after that use in http function.
    .factory('ApiUrl', ['$q', '$http', '$rootScope', function($q, $http, $rootScope) {
        // API_Url for your Web Application.
        // Return this object and pass access to all API urls.



        return {
            // Users:
            wordPressApiUrl: "https://firstunited-events.com/wtc2017/api/",

            // Post types:
            speakersUrl: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/speakers?order=asc&orderby=title&per_page=100", "speakers"],
            speakerUrl: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/speakers/", "speakers"],
            sessionsUrl: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/schedule?per_page=100", "sessions"],
            sessionsByDayUrl: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/schedule?day=", "sessions"],
            sessionUrl: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/schedule/", "sessions"],
            exhibitorsUrl: ["http://13367.de/wp-json/wp/v2/exhibitors-wtc?per_page=100", "exhibitors"],
            exhibitorUrl: ["http://13367.de/wp-json/wp/v2/exhibitors-wtc/", "exhibitors"],
            exhibitorByLevelUrl: ["http://13367.de/wp-json/wp/v2/exhibitors-wtc?categories=", "exhibitors"],
            abstractsUrl: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/abstract?per_page=100", "abstratcts"],
            abstractUrl: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/abstract/", "abstratcts"],

            // Notification
            notificationsUrl: ["http://13367.de/wp-json/wp/v2/notifications?per_page=100", "notifications"],
            notificationUrl: ["http://13367.de/wp-json/wp/v2/notifications/", "notifications"],

            // Taxonomies:
            scheduleDays: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/day", "scheduleDays"],
            scheduleRooms: ["https://firstunited-events.com/wtc2017/wp-json/wp/v2/locations", "scheduleRooms"],

            // QR code API link
            createQRCode: ["https://api.qrserver.com/v1/create-qr-code/?size=150x150&data=", "userQR"],

            //Formidable APIs:
            formidableFormHtml: "http://13367.de/wp-json/frm/v2/forms/39?return=html",
            formidableForm: "http://13367.de/wp-json/frm/v2/forms/15/fields",
            formidableAllForms: "http://13367.de/wp-json/frm/v2/forms",
            sessionFormHtml: "http://13367.de/wp-json/frm/v2/forms/42?return=html",

            // Sponsors category ID:
            levelPlatinum: 22,
            levelGold: 21,
            levelSilver: 20,
            levelBronze: 26,

            // Static Pages
            sitePagesLink: "https://firstunited-events.com/wtc2017/wp-json/wp/v2/pages/",
            welcomeMessagePage: 6235,
            generalInfoPage: 6236,
            safetyPage: 6237,
            itaSchedulePage: 6252,
            contactPage: 6255,
            floorplanImages: 6254,
            programOverview: 6370,
            detailedScheduleModalPage: 7688,
            itaScheduleImagePage: 7690,

            // Woocommerce API Urls:
        };
    }]);