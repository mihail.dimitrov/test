// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js

angular.module('app', ['ionic', 'ngAnimate', 'app.controllers', 'app.routes', 'app.directives', 'app.services', 'tutorials', 'ngSanitize', 'ApiUrlModule', 'FormSevice', 'encodeDecodeBase64', 'authenticationModule', 'userInfo', 'webStorageSevrice', 'bookmarkLocalSevice', 'ngCordova', 'app.filters', 'app.data', 'PageContentService'])

.config(function($ionicConfigProvider, $sceDelegateProvider) {

    $sceDelegateProvider.resourceUrlWhitelist(['self', '*://www.youtube.com/**', '*://player.vimeo.com/video/**']);
    $ionicConfigProvider.views.maxCache(0);
    $ionicConfigProvider.views.forwardCache(true);
})

.run(function($rootScope, $ionicPlatform, $ionicLoading, $ionicPopup, CONFIG, PAYMENT_CONFIG) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
        // OneSignal push notification
        var notificationOpenedCallback = function(jsonData) {
            console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));

            //alert(jsonData.notification.payload.body);
            var alertPopup = $ionicPopup.alert({
                title: 'WTC 2017',
                template: jsonData.notification.payload.body,
                cssClass: 'app-alert',
                okType: 'button-balanced'
            });
        };

        var iosSettings = {};
        iosSettings["kOSSettingsKeyAutoPrompt"] = true;
        iosSettings["kOSSettingsKeyInAppLaunchURL"] = false;

        window.plugins.OneSignal
            .startInit("4bb3e244-d4f2-493b-b00f-b95fd0d8ec81")
            .iOSSettings(iosSettings)
            .inFocusDisplaying(window.plugins.OneSignal.OSInFocusDisplayOption.Notification)
            .handleNotificationOpened(notificationOpenedCallback)
            .endInit();

        // PayPal Configuration
        var clientIDs = {
            "PayPalEnvironmentProduction": PAYMENT_CONFIG.paypal.production_client_id, // not needed while testing
            "PayPalEnvironmentSandbox": PAYMENT_CONFIG.paypal.sandbox_client_id
        };

        window.PayPalMobile.init(
            clientIDs,
            function() {
                window.PayPalMobile.prepareToRender(
                    "PayPalEnvironmentSandbox", // or "PayPalEnvironmentProduction" for production mode
                    new PayPalConfiguration(PAYMENT_CONFIG.paypal.config),
                    function() { console.log("OK, ready to accept payments!"); }
                );
            }
        );

        // Stripe Configuration
        // Stripe.setPublishableKey(PAYMENT_CONFIG.stripe.public_key);
    });

    $.fn.extend({
        animateCss: function(animationName) {
            var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
            this.addClass('animated ' + animationName).one(animationEnd, function() {
                $(this).removeClass('animated ' + animationName);
            });
            return this;
        }
    });
})

/*
  This directive is used to disable the "drag to open" functionality of the Side-Menu
  when you are dragging a Slider component.
*/
.directive('disableSideMenuDrag', ['$ionicSideMenuDelegate', '$rootScope', function($ionicSideMenuDelegate, $rootScope) {
    return {
        restrict: "A",
        controller: ['$scope', '$element', '$attrs', function($scope, $element, $attrs) {

            function stopDrag() {
                $ionicSideMenuDelegate.canDragContent(false);
            }

            function allowDrag() {
                $ionicSideMenuDelegate.canDragContent(true);
            }

            $rootScope.$on('$ionicSlides.slideChangeEnd', allowDrag);
            $element.on('touchstart', stopDrag);
            $element.on('touchend', allowDrag);
            $element.on('mousedown', stopDrag);
            $element.on('mouseup', allowDrag);

        }]
    };
}])

/*
  This directive is used to open regular and dynamic href links inside of inappbrowser.
*/
.directive('hrefInappbrowser', function() {
    return {
        restrict: 'A',
        replace: false,
        transclude: false,
        link: function(scope, element, attrs) {
            var href = attrs['hrefInappbrowser'];

            attrs.$observe('hrefInappbrowser', function(val) {
                href = val;
            });

            element.bind('click', function(event) {

                window.open(href, '_system', 'location=yes');

                event.preventDefault();
                event.stopPropagation();

            });
        }
    };
});