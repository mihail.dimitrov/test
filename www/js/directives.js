angular.module('app.directives', [])

.directive('autoListDivider', ['$timeout', '$rootScope', function($timeout, $rootScope) {
    var lastDivideKey = "";
    $rootScope.$on('change-divide-key', function(event, args) {
        lastDivideKey = "";
    });

    return {

        link: function(scope, element, attrs) {
            // start-time field
            var key = attrs.autoListDividerValue;
            var defaultDivideFunction = function(k) {
                return k.slice(0, 5).toUpperCase();
            };
            var doDivide = function() {
                var divideKey = defaultDivideFunction(key);
                //console.log('+*+*+*+*+*+*+*+*+*+*+*+*+*');
                //console.log('Last key->' + lastDivideKey);
                //console.log('New key->' + divideKey);
                if (divideKey != lastDivideKey) {
                    var contentTr = angular.element("<div class='item item-divider divider-element'>" + divideKey + "</div>");
                    element[0].parentNode.insertBefore(contentTr[0], element[0]);
                }
                lastDivideKey = divideKey;
            };
            $timeout(doDivide, 0);
        }
    };
}])

.directive('autoListDividerByName', ['$timeout', '$rootScope', function($timeout, $rootScope) {
    var lastDivideKeyAlph = "";
    $rootScope.$on('change-divide-key-alph', function(event, args) {
        lastDivideKeyAlph = "";
    });
    return {
        link: function(scope, element, attrs) {
            var key = attrs.autoListDividerByNameValue;
            var defaultDivideFunction = function(k) {
                return k.slice(0, 1).toUpperCase();
            };
            var doDivide = function() {
                var divideKey = defaultDivideFunction(key);
                if (divideKey != lastDivideKeyAlph) {
                    var contentTr = angular.element("<div class='item item-divider divider-element-alph'>" + divideKey + "</div>");
                    element[0].parentNode.insertBefore(contentTr[0], element[0]);
                }
                lastDivideKeyAlph = divideKey;
            };
            $timeout(doDivide, 0);
        }
    };
}])

.directive('bookmarkIcon', ['$timeout', '$rootScope', 'BookmarkStorageSevice', function($timeout, $rootScope, BookmarkStorageSevice) {
        return {
            link: function(scope, element, attrs) {
                // start-time field
                var favoriteItems = BookmarkStorageSevice.getBookmarks("schedule");
                var itemId = parseInt(attrs.bookmarkIconValue);
                var changeBookmarkIcon = function(id) {
                    for (var index = 0; index < favoriteItems.length; index++) {
                        var elem = angular.element(element);
                        if (id === favoriteItems[index].id) {
                            elem.removeClass("ion-android-star-outline");
                            elem.addClass("ion-android-star");
                        }
                    }
                };
                changeBookmarkIcon(itemId);
                //$timeout(changeBookmarkIcon, 0);
            }
        };
    }])
    .directive('speakerBookmarkIcon', ['$timeout', '$rootScope', 'BookmarkStorageSevice', function($timeout, $rootScope, BookmarkStorageSevice) {
        return {
            link: function(scope, element, attrs) {
                // start-time field
                var favoriteItems = BookmarkStorageSevice.getBookmarks("speakers");
                var itemId = parseInt(attrs.speakerBookmarkIconValue);
                var changeBookmarkIcon = function(id) {
                    for (var index = 0; index < favoriteItems.length; index++) {
                        var elem = angular.element(element);
                        if (id === favoriteItems[index].id) {
                            elem.removeClass("ion-android-star-outline");
                            elem.addClass("ion-android-star");
                        }
                    }
                };
                changeBookmarkIcon(itemId);
                //$timeout(changeBookmarkIcon, 0);
            }
        };
    }])
    .directive('exhibitorBookmarkIcon', ['$timeout', '$rootScope', 'BookmarkStorageSevice', function($timeout, $rootScope, BookmarkStorageSevice) {
        return {
            link: function(scope, element, attrs) {
                // start-time field
                var favoriteItems = BookmarkStorageSevice.getBookmarks("exhibitors");
                var itemId = parseInt(attrs.exhibitorBookmarkIconValue);
                var changeBookmarkIcon = function(id) {
                    for (var index = 0; index < favoriteItems.length; index++) {
                        var elem = angular.element(element);
                        if (id === favoriteItems[index].id) {
                            elem.removeClass("ion-android-star-outline");
                            elem.addClass("ion-android-star");
                        }
                    }
                };
                changeBookmarkIcon(itemId);
                //$timeout(changeBookmarkIcon, 0);
            }
        };
    }])
    .directive('slotBookmarkIcon', ['$timeout', '$rootScope', 'BookmarkStorageSevice', function($timeout, $rootScope, BookmarkStorageSevice) {
        return {
            link: function(scope, element, attrs) {
                // start-time field
                var favoriteItems = BookmarkStorageSevice.getBookmarks("slots");
                //var itemId = parseInt(attrs.slotBookmarkIconValue);
                var itemId = attrs.slotBookmarkIconValue;
                var changeBookmarkIcon = function(id) {
                    for (var index = 0; index < favoriteItems.length; index++) {
                        var elem = angular.element(element);
                        if (id === favoriteItems[index].id) {
                            elem.removeClass("ion-android-star-outline");
                            elem.addClass("ion-android-star");
                        }
                    }
                };
                changeBookmarkIcon(itemId);
                //$timeout(changeBookmarkIcon, 0);
            }
        };
    }]);