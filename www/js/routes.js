angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider


        .state('menu.wTC2017', {
        url: '/home',
        views: {
            'side-menu21': {
                templateUrl: 'templates/wTC2017.html',
                controller: 'wTC2017Ctrl'
            }
        }
    })

    .state('menu.schedule', {
        url: '/schedule',
        views: {
            'side-menu21': {
                templateUrl: 'templates/schedule.html',
                controller: 'scheduleCtrl'
            }
        }
    })

    .state('menu', {
        cache: false,
        url: '/side-menu21',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'menuCtrl'
    })

    .state('menu.sponsors', {
        url: '/sponsors',
        views: {
            'side-menu21': {
                templateUrl: 'templates/sponsors.html',
                controller: 'sponsorsCtrl'
            }
        }
    })

    .state('menu.exhibitors', {
        url: '/exhibitors',
        views: {
            'side-menu21': {
                templateUrl: 'templates/exhibitors.html',
                controller: 'exhibitorsCtrl'
            }
        }
    })

    .state('menu.committee', {
        url: '/committee',
        views: {
            'side-menu21': {
                templateUrl: 'templates/committee.html',
                controller: 'committeeCtrl'
            }
        }
    })

    .state('menu.contact', {
        url: '/contact',
        views: {
            'side-menu21': {
                templateUrl: 'templates/contact.html',
                controller: 'contactCtrl'
            }
        }
    })

    .state('menu.safety', {
        url: '/safety',
        views: {
            'side-menu21': {
                templateUrl: 'templates/safety.html',
                controller: 'safetyCtrl'
            }
        }
    })

    .state('menu.floorplan', {
            url: '/floorplan',
            params: {
                id: ""
            },
            views: {
                'side-menu21': {
                    templateUrl: 'templates/floorplan.html',
                    controller: 'floorplanCtrl'
                }
            }
        })
        .state('menu.subfloorplan1', {
            cache: false,
            url: '/floorplan1',
            params: {
                id: ""
            },
            views: {
                'side-menu21': {
                    templateUrl: 'templates/floorplan1.html',
                    controller: 'floorplan1Ctrl'
                }
            }
        })
        .state('menu.subfloorplan2', {
            cache: false,
            url: '/floorplan2',
            params: {
                id: ""
            },
            views: {
                'side-menu21': {
                    templateUrl: 'templates/floorplan2.html',
                    controller: 'floorplan2Ctrl'
                }
            }
        })
        .state('menu.subfloorplan3', {
            cache: false,
            url: '/floorplan3',
            params: {
                id: ""
            },
            views: {
                'side-menu21': {
                    templateUrl: 'templates/floorplan3.html',
                    controller: 'floorplan3Ctrl'
                }
            }
        })

    .state('menu.notifications', {
        url: '/notifications',
        views: {
            'side-menu21': {
                templateUrl: 'templates/notifications.html',
                controller: 'notificationsCtrl'
            }
        }
    })

    .state('menu.search', {
        url: '/search',
        views: {
            'side-menu21': {
                templateUrl: 'templates/search.html',
                controller: 'searchCtrl'
            }
        }
    })

    .state('menu.attendees', {
        url: '/attendees',
        views: {
            'side-menu21': {
                templateUrl: 'templates/attendees.html',
                controller: 'attendeesCtrl'
            }
        }
    })

    .state('menu.attendee', {
        url: '/attendee',
        params: {
            id: ""
        },
        views: {
            'side-menu21': {
                templateUrl: 'templates/attendee.html',
                controller: 'attendeeCtrl'
            }
        }
    })

    .state('menu.activityfeed', {
        url: '/activityfeed',
        views: {
            'side-menu21': {
                templateUrl: 'templates/activityfeed.html',
                controller: 'activityfeedCtrl'
            }
        }
    })

    .state('menu.speakers', {
        url: '/speakers',
        views: {
            'side-menu21': {
                templateUrl: 'templates/speakers.html',
                controller: 'speakersCtrl'
            }
        }
    })

    .state('menu.speaker', {
        url: '/speakerinfo',
        params: {
            id: ""
        },
        views: {
            'side-menu21': {
                templateUrl: 'templates/speaker.html',
                controller: 'speakerCtrl'
            }
        }
    })

    .state('menu.sponsor', {
        url: '/sponsordetails',
        params: {
            id: ""
        },
        views: {
            'side-menu21': {
                templateUrl: 'templates/sponsor.html',
                controller: 'sponsorCtrl'
            }
        }
    })

    .state('menu.session', {
        url: '/session',
        params: {
            id: ""
        },
        views: {
            'side-menu21': {
                templateUrl: 'templates/session.html',
                controller: 'sessionCtrl'
            }
        }
    })

    .state('menu.exhibitor', {
        cache: false,
        url: '/exhibitorsdetails',
        params: {
            id: ""
        },
        views: {
            'side-menu21': {
                templateUrl: 'templates/exhibitor.html',
                controller: 'exhibitorCtrl'
            }
        }
    })

    .state('menu.welcomeMessage', {
            url: '/welcomemessage',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/welcomeMessage.html',
                    controller: 'welcomeMessageCtrl'
                }
            }
        })
        .state('menu.programOverview', {
            url: '/programOverview',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/programOverview.html',
                    controller: 'programOverviewCtrl'
                }
            }
        })

    .state('menu.myAccount', {
        url: '/myaccount',
        views: {
            'side-menu21': {
                templateUrl: 'templates/myAccount.html',
                controller: 'myAccountCtrl'
            }
        }
    })

    .state('menu.news', {
        url: '/news',
        views: {
            'side-menu21': {
                templateUrl: 'templates/news.html',
                controller: 'newsCtrl'
            }
        }
    })

    .state('menu.survey', {
        url: '/survey',
        views: {
            'side-menu21': {
                templateUrl: 'templates/survey.html',
                controller: 'surveyCtrl'
            }
        }
    })


    .state('menu.notificationDetails', {
        url: '/notificationDetails',
        params: {
            id: ""
        },
        views: {
            'side-menu21': {
                templateUrl: 'templates/notificationDetails.html',
                controller: 'notificationDetailsCtrl'
            }
        }
    })

    .state('videos', {
        url: '/video',
        templateUrl: 'templates/videos.html',
        controller: 'videosCtrl'
    })

    .state('menu.bookmarks', {
        url: '/bookmarks',
        views: {
            'side-menu21': {
                templateUrl: 'templates/bookmarks.html',
                controller: 'bookmarksCtrl'
            }
        }
    })

    .state('menu.mySlots', {
        url: '/myslots',
        views: {
            'side-menu21': {
                templateUrl: 'templates/myScheduleSlots.html',
                controller: 'mySlotsCtrl'
            }
        }
    })

    .state('gallery', {
        url: '/gallery',
        templateUrl: 'templates/gallery.html',
        controller: 'galleryCtrl'
    })

    .state('video', {
        url: '/singlevideo',
        params: {
            videokey: ""
        },
        templateUrl: 'templates/video.html',
        controller: 'videoCtrl'
    })

    .state('menu.abstract', {
        url: '/abstract',
        params: {
            id: ""
        },
        views: {
            'side-menu21': {
                templateUrl: 'templates/abstract.html',
                controller: 'abstractCtrl'
            }
        }
    })

    .state('menu.googleMap', {
        url: '/googlemap',
        views: {
            'side-menu21': {
                templateUrl: 'templates/googleMap.html',
                controller: 'googleMapCtrl'
            }
        }
    })

    .state('menu.wpLogin', {
        url: '/wplogin',
        views: {
            'side-menu21': {
                templateUrl: 'templates/wpLogin.html',
                controller: 'wpLoginCtrl'
            }
        }
    })

    .state('menu.forgotPassword', {
        url: '/forgotpass',
        views: {
            'side-menu21': {
                templateUrl: 'templates/forgotPassword.html',
                controller: 'forgotPasswordCtrl'
            }
        }
    })

    .state('menu.registration', {
        url: '/registration',
        views: {
            'side-menu21': {
                templateUrl: 'templates/registration.html',
                controller: 'registrationCtrl'
            }
        }
    })

    .state('menu.formidableForm', {
        url: '/formidableform',
        views: {
            'side-menu21': {
                templateUrl: 'templates/formidableForm.html',
                controller: 'formidableFormCtrl'
            }
        }
    })

    .state('menu.exhibitorsMap', {
        url: '/exhibitorsmap',
        views: {
            'side-menu21': {
                templateUrl: 'templates/exhibitorsMap.html',
                controller: 'exhibitorsMapCtrl'
            }
        }
    })

    .state('menu.iTASchedule', {
        url: '/itaschedule',
        views: {
            'side-menu21': {
                templateUrl: 'templates/iTASchedule.html',
                controller: 'iTAScheduleCtrl'
            }
        }
    })

    .state('menu.mySchedule', {
        url: '/myschedule',
        views: {
            'side-menu21': {
                templateUrl: 'templates/mySchedule.html',
                controller: 'myScheduleCtrl'
            }
        }
    })

    .state('menu.info', {
        url: '/programOverview',
        templateUrl: 'templates/programOverview.html',
        controller: 'programOverviewCtrl'
    })

    .state('menu.generalInformation', {
            url: '/generalinformation',
            views: {
                'side-menu21': {
                    templateUrl: 'templates/generalInformation.html',
                    controller: 'generalInformationCtrl'
                }
            }
        })
        .state('menu.products', {
            url: "/products",
            views: {
                'side-menu21': {
                    templateUrl: "templates/products-grid-2.html",
                    controller: 'ProductsCtrl'
                }
            }
        })

    .state('menu.product', {
            url: "/products/:product_id",
            views: {
                'side-menu21': {
                    templateUrl: "templates/product.html",
                    controller: 'ProductCtrl'
                }
            }
        })
        .state('menu.basket', {
            url: "/basket",
            views: {
                'side-menu21': {
                    templateUrl: "templates/basket.html",
                    controller: 'BasketCtrl'
                }
            }
        })
        .state('menu.payment', {
            url: "/payment",
            views: {
                'side-menu21': {
                    templateUrl: "templates/payment.html",
                    controller: 'PaymentCtrl'
                }
            }
        })
        .state('menu.newCustomer', {
            url: "/newCustomer",
            views: {
                'side-menu21': {
                    templateUrl: "templates/new-customer.html",
                    controller: 'NewCustomerCtrl'
                }
            }
        })
        // .state('menu.orders', {
        //     url: "/orders/:customer_id",
        //     views: {
        //         'side-menu21': {
        //             templateUrl: "templates/orders.html",
        //             controller: 'OrdersCtrl'
        //         }
        //     }
        // })
        .state('menu.orders', {
            cache: false,
            url: "/orders",
            views: {
                'side-menu21': {
                    templateUrl: "templates/orders.html",
                    controller: 'OrdersCtrl'
                }
            }
        })
        .state('menu.orderdetail', {
            cache: false,
            url: "/orders/:order_id",
            views: {
                'side-menu21': {
                    templateUrl: "templates/orderdetail.html",
                    controller: 'OrderdetailCtrl'
                }
            }
        })

    $urlRouterProvider.otherwise('/side-menu21/home')


});