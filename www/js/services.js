angular.module('app.services', ['ionic'])
    .value('currentScheduleDayId1', 71)
    .value('activeSection1', 1)
    .config(function($ionicConfigProvider) {
        $ionicConfigProvider.views.maxCache(0);
        $ionicConfigProvider.views.forwardCache(true);
    })
    .factory('PostTypeService', ['$q', '$http', '$rootScope', '$ionicLoading', '$ionicPopup', '$state', 'WebStorageSevice', 'BookmarkStorageSevice', function($q, $http, $rootScope, $ionicLoading, $ionicPopup, $state, WebStorageSevice, BookmarkStorageSevice) {
        // With this function we get all data for current post type from WordPress DB.
        var pageCounter = 0;

        function getPageCounter() {
            return pageCounter;
        }

        function setPageCounter() {
            pageCounter = 0;
            //currentSearchWord = searchWord;
        }


        function getAllPosts(postApiUrl) {
            var deferred = $q.defer();
            $http.get(postApiUrl[0]).then(function(response) {
                //deferred.resolve(WebStorageSevice.getAllPostsFromWebStorage(response.data, postApiUrl[1]));
                deferred.resolve(response);
            }, function(response) {
                deferred.resolve(false);
            });
            return deferred.promise;
        }

        function getPostsByDay(postApiUrl) {
            var deferred = $q.defer();
            $http.get(postApiUrl).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.resolve(false);
            });
            return deferred.promise;
        }
        // With this function we get all information for current post item from WordPress DB.
        function getPostDetails(postApiUrl, singlePostId) {
            var deferred = $q.defer();
            // Get details information from local storage
            // var postDetails = WebStorageSevice.getDataFromWebStorage(postApiUrl[1]);

            // _.each(postDetails, function(val, key) {
            //     if (postDetails[key].id == singlePostId) {
            //         console.log(postDetails[key]);
            //         deferred.resolve(postDetails[key]);
            //     }
            // });

            // Get details information from WP DB.
            var currentPostApiUrl = postApiUrl[0] + singlePostId;
            $http.get(currentPostApiUrl).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.resolve(false);
            });

            return deferred.promise;
        }
        // Make POST HTTP request.
        // Example data ==> { "title": "My first post!" };
        function addPostItem(postApiUrl, postObject) {
            var deferred = $q.defer();
            $http.post(postApiUrl, postObject).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.resolve(false);
            });
            return deferred.promise;
        }

        function bookmarkPost(post) {
            BookmarkStorageSevice.bookmarkPost(post);
            $rootScope.$broadcast("new-bookmark", post);
        }

        function getFirst10Posts(postApiUrl) {
            var deferred = $q.defer();
            var url = postApiUrl[0] + "?order=asc&orderby=title&per_page=50";

            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.resolve(false);
            });

            return deferred.promise;
        }

        function getNext10Posts(postApiUrl) {
            var deferred = $q.defer();
            pageCounter += 50;
            var url = postApiUrl[0] + "?order=asc&orderby=title&offset=" + pageCounter + "&per_page=50";
            //console.log(url);
            $http.get(url).then(function(response) {
                deferred.resolve(response.data);
            }, function(response) {
                deferred.resolve(false);
            });
            return deferred.promise;

        }

        function getAllPostsFromNextPage(postApiUrl, pageNumber) {
            var url = postApiUrl[0] + "&page=" + pageNumber;
            //console.log(url);
            var deferred = $q.defer();
            $http.get(url).then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.resolve(false);
            });
            return deferred.promise;
        }

        function callBackError() {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Something went wrong!',
                //template: 'Successfully added!',
                cssClass: 'app-alert',
                okType: 'button-balanced'
            });
            alertPopup.then(function(res) {
                $state.go('menu.wTC2017');
            });
        }

        function callBackErrorDetails() {
            $ionicLoading.hide();
            var alertPopup = $ionicPopup.alert({
                title: 'Something went wrong!',
                //template: 'Successfully added!',
                cssClass: 'app-alert',
                okType: 'button-balanced'
            });
            alertPopup.then(function(res) {
                //$state.go('menu.wTC2017');
            });
        }

        function getTaxonomy(taxApiUrl) {
            var deferred = $q.defer();
            $http.get(taxApiUrl[0]).then(function(response) {
                //deferred.resolve(WebStorageSevice.getAllPostsFromWebStorage(response.data, postApiUrl[1]));
                deferred.resolve(response);
            }, function(response) {
                deferred.resolve(false);
            });
            return deferred.promise;
        }

        function getNotification(postApiUrl) {
            var deferred = $q.defer();
            var url = postApiUrl[0] + '&' + Math.random();
            $http.get(url).then(function(response) {
                deferred.resolve(response);
            }, function(response) {
                deferred.resolve(false);
            });
            return deferred.promise;
        }

        function getAbstractDetails(postApiUrl, singlePostId) {
            //console.log('------IN SERVICE-------')
            var deferred = $q.defer();
            //console.log('id->' + singlePostId);
            // Get details information from WP DB.
            var currentPostApiUrl = postApiUrl[0] + singlePostId;
            $http.get(currentPostApiUrl).then(function(response) {
                //console.log('------ok-------');
                deferred.resolve(response);
            }, function(response) {
                //console.log('------bad-------');
                //console.log(response);
                deferred.resolve(response);
            });

            return deferred.promise;
        }

        // Return this object and pass service functionalities to controller.
        // Use this in controller.
        var ret = {
            getPosts: getAllPosts,
            getPostsByDay: getPostsByDay,
            getPost: getPostDetails,
            addPost: addPostItem,
            bookmarkPost: bookmarkPost,
            getFirst10Posts: getFirst10Posts,
            getNext10Posts: getNext10Posts,
            getAllPostsFromNextPage: getAllPostsFromNextPage,
            callBackError: callBackError,
            callBackErrorDetails: callBackErrorDetails,
            getTaxonomy: getTaxonomy,
            getNotification: getNotification,
            getAbstractDetails: getAbstractDetails,
            getPageCounter: getPageCounter,
            setPageCounter: setPageCounter
        };
        // Return this variable and use in controller.
        return ret;
    }])
    .factory('MainVariableService', ['$q', '$http', '$rootScope', '$ionicLoading', '$ionicPopup', '$state', function($q, $http, $rootScope, $ionicLoading, $ionicPopup, $state) {
        // Schedule
        var currentScheduleDayId = 71;
        var currentScheduleSection = 1;
        // Sponsors
        var currentSponsorLevel = 22;
        var currentSponsorSection = 1;
        // Search
        var currentSearchData = [];
        var currentSearchWord = "";

        // Schedule
        function getScheduleCurrentDay() {
            return currentScheduleDayId;
        }

        function setScheduleCurrentDay(newDayID) {
            if (currentScheduleDayId !== newDayID) {
                currentScheduleDayId = newDayID;
            }
        }

        function getScheduleCurrentSection() {
            return currentScheduleSection;
        }

        function setScheduleCurrentSection(newSectionID) {
            if (currentScheduleSection !== newSectionID) {
                currentScheduleSection = newSectionID;
            }
        }

        // Sponsors
        function getSponsorsCurrentLevel() {
            return currentSponsorLevel;
        }

        function setSponsorsCurrentLevel(newLevelID) {
            if (currentSponsorLevel !== newLevelID) {
                currentSponsorLevel = newLevelID;
            }
        }

        function getSponsorCurrentSection() {
            return currentSponsorSection;
        }

        function setSponsorCurrentSection(newSectionID) {
            if (currentSponsorSection !== newSectionID) {
                currentSponsorSection = newSectionID;
            }
        }

        function getSearchData() {
            return currentSearchData;
        }

        function setSearchData(searchData) {
            currentSearchData = [];
            currentSearchData = searchData;
        }

        function getSearchWord() {
            return currentSearchWord;
        }

        function setSearchWord(searchWord) {
            currentSearchWord = "";
            currentSearchWord = searchWord;
        }

        return {
            getScheduleCurrentDay: getScheduleCurrentDay,
            setScheduleCurrentDay: setScheduleCurrentDay,
            getScheduleCurrentSection: getScheduleCurrentSection,
            setScheduleCurrentSection: setScheduleCurrentSection,
            getSponsorsCurrentLevel: getSponsorsCurrentLevel,
            setSponsorsCurrentLevel: setSponsorsCurrentLevel,
            getSponsorCurrentSection: getSponsorCurrentSection,
            setSponsorCurrentSection: setSponsorCurrentSection,
            getSearchData: getSearchData,
            setSearchData: setSearchData,
            getSearchWord: getSearchWord,
            setSearchWord: setSearchWord
        };
    }]);